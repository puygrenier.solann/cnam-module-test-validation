import { Component, OnInit,Input } from '@angular/core';
import { Status,AdminRequest } from '../model/admin-request.model'
import { Item } from '../model/items-model.model';
import { RequestUpdateService } from "../service/request-update.service"

@Component({
  selector: 'app-request-summary',
  templateUrl: './request-summary.component.html',
  styleUrls: ['./request-summary.component.css']
})
export class RequestSummaryComponent implements OnInit {
    public showValidationButton:boolean = true;
    public ShowLoadingButton:boolean = false;
    public ShowSuccesMessage:boolean = false;
    public ShowErrorMessage:boolean = false;
    public UpdateSend: boolean = false
    public messageToShow:string = "";
    public statustoUpdate:Status = Status.Undefined;

    public responeToRequest = {
      200: "Succefull request status update",
      204: "Ressources to update not found",
      404: "Cannot find request to update",
      409: "Cannot change a request status",
      500: "Something went wrong on the server side"
    }

  @Input() request:AdminRequest = new AdminRequest("","",new Item("","","",""),0,Status.InProgress);
  @Input() activatedOption:boolean = false;
  constructor(private serviceUpdater: RequestUpdateService) { }

  ngOnInit(): void {
  }

  Validate(){
    this.statustoUpdate = Status.Validated;
  }

  Cancel(){
    this.statustoUpdate = Status.Canceled;
  }

  UpdateRequest(){
    this.LoadingButtonActivate()
    this.serviceUpdater.UpdatePurse(this.request.convertToUserRequest(),this.statustoUpdate).subscribe(
      (response) => {
        console.log(JSON.stringify(response))
         this.SuccesMessageActivate();
         this.HideLoadingButton()
         this.messageToShow = this.responeToRequest[200];
      },
      (not200response) => {
        if(not200response.status === 204)this.messageToShow = this.responeToRequest[204];
        if (not200response.status === 404)this.messageToShow = this.responeToRequest[404];
        if (not200response.status === 500)this.messageToShow = this.responeToRequest[500];
        if (not200response.status === 409)this.messageToShow = this.responeToRequest[409];
        else this.messageToShow = this.responeToRequest[409];
        this.ErrorMessageActivate();
        this.HideLoadingButton()
      },
      () => {}
    );
  }

  private LoadingButtonActivate(){
    this.showValidationButton = false;
    this.ShowLoadingButton = true;
  }

  private HideLoadingButton(){
    this.ShowLoadingButton = false;
  }

  private SuccesMessageActivate(){
    this.ShowSuccesMessage = true;
    this.ShowErrorMessage = false;
    this.UpdateSend=true;
    this.activatedOption = false;
  }

  private ErrorMessageActivate(){
    this.ShowSuccesMessage = false;
    this.ShowErrorMessage = true;
    this.UpdateSend=true;
    this.activatedOption = false;
  }

}
