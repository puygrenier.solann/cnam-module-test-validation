import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from "@angular/common/http";
import { UserModel,UserAdapter } from '../model/user-model.model';
import { Observable} from 'rxjs';
import { map } from 'rxjs';

const headers = new HttpHeaders()
            .set("Access-Control-Allow-Origin", "*")
            .set('Content-Type','application/json');

@Injectable({
  providedIn: 'root'
})
export class UserListingService {

  constructor(private http:HttpClient, private adapter:UserAdapter ) { }

  GetAllUser(): Observable<UserModel[]>{
    const url="https://localhost:7210/api/CofeeManagerUser/user/all";
    console.log(url);
    return this.http
      .get<any>(url,{headers: headers      }).pipe(
        map((data:any) => data.map( (user:any) => this.adapter.adaptUser(user)))
      );
  }
}
