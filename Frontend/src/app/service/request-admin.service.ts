import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Status, AdminRequest, AdminRequestAdaptater } from '../model/admin-request.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs';

const headers = new HttpHeaders()
            .set("Access-Control-Allow-Origin", "*")
            .set('Content-Type','application/json');

@Injectable({
  providedIn: 'root'
})
export class RequestAdminService {

  constructor(private http: HttpClient,private adapter:AdminRequestAdaptater) { }

  AccesAllRequestByStatus(status:Status) : Observable<AdminRequest[]>{
    const url="https://localhost:7210/api/CofeeManagerRequest/request/all/"+ status;
    return this.http
      .get<any>(url).pipe(
        map((data: any) => data.map((request: any) => this.adapter.adapt(request)))
      );
  }



}
