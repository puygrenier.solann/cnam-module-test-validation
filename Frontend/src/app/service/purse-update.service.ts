import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from "@angular/common/http";
import { UserModel,UserAdapter } from '../model/user-model.model';
import { Observable} from 'rxjs';
import { map } from 'rxjs';

const headers = new HttpHeaders()
            .set("Access-Control-Allow-Origin", "*")
            .set('Content-Type','application/json');

@Injectable({
  providedIn: 'root'
})
export class PurseUpdateService {

  constructor(private http:HttpClient) { }

  IncreasePurseOfUser(id:string,value:number){
    const url="https://localhost:7210/api/CofeeManagerUser/user/" +id +"/purse/add/"+value;
    var body = {};
    return this.http
      .put<any>(url,body,{headers: headers});
  }

  DecreasePurseOfUser(id:string,value:number){
    const url="https://localhost:7210/api/CofeeManagerUser/user/" +id +"/purse/remove/"+value;
    var body = {};
    return this.http
      .put<any>(url,body,{headers: headers});
  }
}
