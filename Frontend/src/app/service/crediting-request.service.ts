import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient,HttpHeaders } from "@angular/common/http";
import { map } from 'rxjs';
import { filter } from 'rxjs';

import { UserRequestAdapter } from '../model/user-request.model';
import { UserRequest } from '../model/user-request.model';

const headers = new HttpHeaders()
            .set("Access-Control-Allow-Origin", "*")
            .set('Content-Type','application/json');

@Injectable({
  providedIn: 'root'
})
export class CreditingRequestService {

  constructor(private http: HttpClient, private adapter:UserRequestAdapter) {
  }

  CreateRequest(userId:string,requestedItemId:string,quantity:number ): Observable<string> {
    console.log("userId: ${userId}, requestedItemId: ${requestedItemId}");
    const url="https://localhost:7210/api/CofeeManagerRequest/request";
    var body = {
                  "userId": userId,
                  "requestedItem": requestedItemId,
                  "quantity": quantity
                };
    console.log(body);
    return this.http
      .post<any>(url, body, {
        headers: headers
        //params: params,
      });//.subscribe((res) => console.log(res));
    }

    AccesRequest(userId:string) : Observable<UserRequest[]>{
      let url="https://localhost:7210/api/CofeeManagerRequest/request/user/"+userId;
      return this.http.get<any>(url, {
        headers: headers
      }).pipe (
        map((data: any) => data.map((item: any) => this.adapter.adapt(item)))
      );
      /*return this.http.get(url).pipe(
        map((data: any) => data.map((item: any) => this.adapter.adapt(item)))*/
    }

    AccesRequestInProgress(userId:string){
      let url="https://localhost:7210/api/CofeeManagerRequest/request/user/"+userId;
      return this.http.get<any>(url, {
        headers: headers
      }).pipe (
        //TO FINISH
        map((data: any) => data.map((item: any) => this.adapter.adapt(item)))
      );
    }
    AccesRequestValidated(userId:string){
//TODO
    }
    AccesRequestCanceled(userId:string){
//TODO
    }
}
