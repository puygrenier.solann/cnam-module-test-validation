import { TestBed } from '@angular/core/testing';

import { PurseUpdateService } from './purse-update.service';

describe('PurseUpdateService', () => {
  let service: PurseUpdateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PurseUpdateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
