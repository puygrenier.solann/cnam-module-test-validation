import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'coffee-managementFront';
  temp:any="";
  notYetAuthentified:boolean=true;

  IsAuthentified(data:any):string{
    console.log(JSON.stringify(data));
    this.temp =  window.sessionStorage.getItem("token");
    if ( ! window.sessionStorage.getItem("token")) {
      this.notYetAuthentified=false
      return "bi bi-exclamation-lg";
    }
    else {
      this.notYetAuthentified=false
      return "bi bi-check2-circle" ;

    }


  }
}
