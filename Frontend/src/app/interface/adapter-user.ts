export interface AdapterUser<T> {
    adapt(user: any): T;
}

export interface AdapterUserData<T> {
    adaptUser(user: any): T;
}
