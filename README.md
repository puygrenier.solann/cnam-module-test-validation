# Development note

## Initialisation
  1) clone the repository  
  2) start API using visual studio execution button (you may have to set Web_API as startup project)  
  3) You can use Swager

## Vocabulary to use
| FR | AN |
|----|-----|
|  |  |
|  |  |
|  |  |


## TODO
 * Linter C#
 * Continuous Integration (CI)
 * Continuous Deployement (CD)

## Coding convention

Clean code must be used.  
 
##### TDD
 * For Red stage please use `[Trait("Category", "TDD Red")]`
 * For Green stage please use `[Trait("Category", "Title of test")]`
 * For Refacto stage please use `[Theory(Skip = true)]` or `[Fact(Skip=true)]`

## Language and IDE used
  - Back End
    - C# using VisualStudio 2022 (*Framework .NET 6.0*)
      - api (with swager)
      - Xunit
      - model
  - Front End
    - In progress
  - Git using TortoiseGit

# Deployement ([debian 11](https://docs.microsoft.com/en-us/dotnet/core/install/linux-debian#debian-11-) )

In progress regarding CD ..
```[sh]
wget https://packages.microsoft.com/config/debian/11/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
dpkg -i packages-microsoft-prod.deb
rm packages-microsoft-prod.deb
apt-get update; \
  apt-get install -y apt-transport-https && \
  apt-get update && \
  apt-get install -y dotnet-sdk-6.0
apt-get update; \
  apt-get install -y apt-transport-https && \
  apt-get update && \
  apt-get install -y aspnetcore-runtime-6.0

chmod +x <name-publication>
./Web_API --urls http://localhost:5519
```



---

# Usefull ressources / Memo

## bash
used script for transfert from current pc to distant linux : `scp ./source root@167.71.14.244:/media/scp/`

## Markdown
 - [Online Markdown editor & samples ][MrkLink]
 - We have *Italic* (\*Italic\*), **Bold**  (\*\*Bold\*\*) or incrust code `like that`(with \` \` )
 - block of code are made with <br>\`\`\`[\[sh or python or ...\]][MarkListLang]<br>[...]<br> \`\`\`
 - Citation can be made with \> or >> or >>> or ...
 - Add picture using \[ \! \[ Solid \] \( link.png \) \] \( link \) or \[ ! \](link.png)
 - Link using ( text )[ link ] or  [ text ][ markdown_link ]
 - return cariage can be done using \<br> or two space folowed by \
 - line can be done using ---

# Additional based ressource
| Title | Link |
| --- | --- |
| Onion architecture | [StackExchange][StackOnionArch] |
| Tutorial C# API  | [Youtube](https://www.youtube.com/watch?v=Fbf_ua2t6v4&t=17s)
| Good practice for API design | [Youtube](https://www.youtube.com/watch?v=vyHpbR6jScI) |
| C# Exception | [Microsoft Documentation Exception][MicrosoftIllegalArg] |
| C# Primitiv type | [Microsoft Documentation build-in type][MicrosoftType] |
| xunit in parallel | [Running test in parallel][xunitParalel] |


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)
 [MrkLink]: <https://dillinger.io/>
 [MarkListLang]: <https://markdown.land/markdown-code-block#markdown-code-block-language-list>
 [DigitOcean]: <https://cloud.digitalocean.com/projects/136927ed-2272-4859-95c2-7c8b6bc9ad00/resources?i=193923>
 [MicrosoftIllegalArg]: <https://docs.microsoft.com/en-us/dotnet/standard/exceptions/>
 [StackOnionArch]: <https://softwareengineering.stackexchange.com/questions/373406/onion-architecture-layer-placement-of-business-logic>
 [MicrosoftType]: <https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/built-in-types>
 [xunitParalel]: <https://xunit.net/docs/running-tests-in-parallel>
