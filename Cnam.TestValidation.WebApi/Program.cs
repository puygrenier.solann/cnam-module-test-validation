using Cnam.TestValidation.Configuration;
using Cnam.TestValidation.WebApi;
using Microsoft.OpenApi.Models;
using System;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container + enum as string.
builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(
        policy =>
        {
            policy.WithOrigins("https://localhost:7210/").AllowAnyHeader()
                                                  .AllowAnyMethod()
                                                  .AllowAnyOrigin();
        });
});
builder.Services.AddControllers().AddJsonOptions(options =>
            options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter())); ;
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen( swagerOption =>
    {
        swagerOption.SwaggerDoc("v1", new OpenApiInfo
        {
            Version = "v1",
            Title = "Cofee Manageur",
            Description = "A web API for interacting with the model",
            Contact = new OpenApiContact { Email = "spuygrenier@cfai-formation.fr", Name = "Meyer Arthur, Welsh Th�o\n, Hammami Ines, Puygrenier Solann" }
        });
        
        var filePath = Path.Combine(AppContext.BaseDirectory, "ApiControllerDocumentation.xml");
        swagerOption.IncludeXmlComments(filePath);
    }
);

if (builder.Environment.IsProduction())
{
    System.Environment.SetEnvironmentVariable("ConnectionString", builder.Configuration.GetConnectionString("ProductionConnection"));
    SharedConfigurationDataOfTheApplication.ConnectionString = builder.Configuration.GetConnectionString("ProductionConnection");
}
else if (builder.Environment.IsDevelopment())
{
    System.Environment.SetEnvironmentVariable("ConnectionString", builder.Configuration.GetConnectionString("StaggingConnection"));
    SharedConfigurationDataOfTheApplication.ConnectionString = builder.Configuration.GetConnectionString("StaggingConnection");
}
else 
{
    System.Environment.SetEnvironmentVariable("ConnectionString", builder.Configuration.GetConnectionString("DefaultConnection"));
    SharedConfigurationDataOfTheApplication.ConnectionString = builder.Configuration.GetConnectionString("DefaultConnection");
}

var app = builder.Build();


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();

}
else {

}


app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();

app.UseCors();
app.UseAuthorization();

app.MapControllers();

app.Run("https://localhost:7210/");
