﻿using Cnam.TestValidation.DomainService;
using Cnam.TestValidation.DomainService.CofeeUseCaseException;
using Cnam.TestValidation.Infrastructure;
using Cnam.TestValidation.Model;
using Cnam.TestValidation.WebApi.WebServiceBody;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Cnam.TestValidation.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CofeeManagerRequestController : ControllerBase
    {
        private readonly ILogger<CofeeManagerUserController> _logger;
        public IDataExchange postGreInfrastructure = new PostgreInfrastructure();


        /// <summary>
        /// Allow to create customer goods request (in progress status)
        /// </summary>
        /// <response code="201">Succefull request creation :  reply contain the REQUEST ID</response>
        /// <response code="409">Cannot make a request : not enought money</response>
        /// <response code="404">User or Item unknow</response>
        /// <response code="400">Error are in the request (negativ quantity)</response>
        /// <response code="500">Something went wrong on the server side</response>
        /// 
        [HttpPost]
        [Route("request")]
        public IActionResult AddRequest(RequestPostBodyRequest request)
        {
            CofeeUseCase cofeeUseCase = new(postGreInfrastructure);
            try
            {
                User user = cofeeUseCase.GetUser(request.UserId);
                Item item = cofeeUseCase.GetItem(request.RequestedItem);
                Guid createdRequest = cofeeUseCase.CreateRequestItemOfUser(user, item,request.Quantity);
                return Created("",createdRequest);
            }
            catch (CofeeUseCaseRequestCreationException ex)
            {
                return Conflict(JsonConvert.SerializeObject(ex));
            }
            catch (CofeeUseCaseItemAccesException ex)
            {
                return NotFound(JsonConvert.SerializeObject(ex));
            }
            catch (CofeeUseCaseUserAccesException ex)
            {
                return NotFound(JsonConvert.SerializeObject(ex));
            }
            catch (CofeeUseCaseUserPurseUpdateException ex)
            {
                _logger.LogCritical(ex.Message);
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                return BadRequest(JsonConvert.SerializeObject(ex));
            }
        }
        
        
        /// <summary>
        /// Provide data regarding all request to solve
        /// </summary>
        /// <response code="200">Return a list of all request</response>
        [HttpGet]
        [Route("request/all/{status}")]
        public IActionResult GetRequest([FromRoute] RequestStatus status)
        {
            CofeeUseCase cofeeUseCase = new(postGreInfrastructure);
            return Ok(cofeeUseCase.GetRequestWith(status));
        }

        /// <summary>
        /// Provide data regarding all request to solve
        /// </summary>
        /// <response code="200">Return a list of all request</response>
        /// <response code="400">User not found</response>
        [HttpGet]
        [Route("request/user/{userId}")]
        public IActionResult GetRequest([FromRoute] Guid userId)
        {
            CofeeUseCase cofeeUseCase = new(postGreInfrastructure);
            try
            {
                User user = cofeeUseCase.GetUser(userId);
                return Ok(cofeeUseCase.GetRequestWith(user));
            }
            catch (CofeeUseCaseUserAccesException ex)
            {
                return NotFound(ex);
            }
        }

        /// <summary>
        /// Provide data regarding all request to solve
        /// </summary>
        /// <response code="200">Return a list of all request</response>
        /// <response code="400">User not found</response>
        [HttpGet]
        [Route("request/user/{userId}/status/{status}")]
        public IActionResult GetRequest([FromRoute] Guid userId,[FromRoute] RequestStatus status)
        {
            CofeeUseCase cofeeUseCase = new(postGreInfrastructure);
            try
            {
                User user = cofeeUseCase.GetUser(userId);
                return Ok(cofeeUseCase.GetRequestWith(user,status));
            }
            catch (CofeeUseCaseUserAccesException ex)
            {
                return NotFound(ex);
            }
        }

        /// <summary>
        /// Allow to change status of a request (from progress to validated or cancel)
        /// </summary>
        /// <response code="200">Succefull request status update</response>
        /// <response code="204">Ressources to update not found</response>
        /// <response code="409">Cannot change a request status</response>
        /// <response code="404">Cannot find request to update</response>
        /// <response code="500">Something went wrong on the server side</response>
        [HttpPut]
        [Route("request/{idRequest}")]
        public IActionResult ChangeRequestStatus([FromRoute] Guid idRequest, RequestStatus status)
        {
            CofeeUseCase cofeeUseCase = new(postGreInfrastructure);
            try
            {
                UserItemRequest request = cofeeUseCase.GetRequestWith(idRequest);
                if (status is RequestStatus.Validated) cofeeUseCase.ValidateRequest(request);
                else if (status is RequestStatus.Canceled) cofeeUseCase.CancelRequest(request);
                return Ok();
            }
            catch (CofeeUseCaseRequestAccesExeption ex)
            {
                return NotFound(ex);
            }
            catch (CofeeUseCaseRequestUpdateException ex)
            {
                return Conflict(ex);
            }
            catch (CofeeUseCaseUserPurseUpdateException ex)
            {
                _logger.LogCritical(ex.Message);
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
            catch ( Exception ex)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }    
    }
}
