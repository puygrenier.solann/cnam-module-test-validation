﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Cnam.TestValidation.Model
{
    public class User : IPurseAction , IEquatable<User>
    {
        public Guid Guid { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }
        public UserPurse UserPurse { get; }

        public User(Guid guid, string userName, string userPassword, decimal userPurseValue)
        {
            Guid = guid;
            this.UserName = userName;
            this.UserPassword = userPassword;
            this.UserPurse = new UserPurse(userPurseValue);
        }
        public User(string userName, string userPassword, decimal userPurseValue) : this(Guid.NewGuid(), userName, userPassword,userPurseValue) { }
        public User(string userName, string userPassword) : this(Guid.NewGuid(),userName, userPassword, 0m) {}
        public User(Guid guid,string userName, string userPassword) : this(guid,userName, userPassword, 0m) { }

        public bool CanBuy(decimal price)
        {
            return UserPurse.CanBuy(price);
        }
        public bool RemoveCoins(decimal credit)
        {
            return UserPurse.RemoveCoins(credit);
        }
        public void AddCoins(decimal credit)
        {
            UserPurse.AddCoins(credit);
        }
        public bool Equals(User? other)
        {
            return (other is not null)
                && UserName == other.UserName
                && UserPassword == other.UserPassword
                && UserPurse.CoinPurse == other.UserPurse.CoinPurse
                && Guid == other.Guid;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}