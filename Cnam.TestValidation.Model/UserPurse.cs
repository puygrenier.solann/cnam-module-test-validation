﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cnam.TestValidation.Model
{
    public enum PurseAction
    {
        Add,
        Remove
    }
    public interface IPurseAction
    {
        public bool CanBuy(decimal price);
        public bool RemoveCoins(decimal credit);
        public void AddCoins(decimal credit);
    }

    public class UserPurse : IPurseAction
    {
        //TODO : Only get ?
        public decimal CoinPurse { get; set; }

        public UserPurse() : this(0) {}

        public UserPurse(decimal value)
        {
            CoinPurse = value;
        }

        public bool CanBuy(decimal price)
        {
            return CoinPurse >= price;
        }

        public bool RemoveCoins(decimal credit)
        {
            if (IsOnlyTwoDigitForCents(credit)) throw new ArgumentOutOfRangeException("Credit value cannot have more than 2 digits place for cents");
            else if (!CanBuy(credit)) throw new ArgumentOutOfRangeException("Remaining credit cannot be negativ");
            else if (CanBuy(credit))
            {
                this.CoinPurse -= credit;
                return true;
            }
            else return false;
        }

        public void AddCoins(decimal credit)
        {
            if (IsOnlyTwoDigitForCents(credit)) throw new ArgumentOutOfRangeException("Credit value cannot have more than 2 digits place for cents");
            else if(credit<0) throw new ArgumentOutOfRangeException("Credit value cannot have more than 2 digits place for cents");
            else this.CoinPurse += credit;
        }


        private static bool IsOnlyTwoDigitForCents(decimal credit)
        {
            return Decimal.Round(credit, 2) != credit;
        }
    }
}
