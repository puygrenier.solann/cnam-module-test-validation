﻿using Cnam.TestValidation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cnam.TestValidation.Infrastructure
{
    public class InMemoryInfrastructure : IDataExchange
    {
        private List<User> allUsers = new();
        private List<Item> allItems = new();
        private List<UserItemRequest> allUserItemRequest = new();

        public InMemoryInfrastructure() {}

        public bool CancelItemUserRequest(UserItemRequest request)
        {
            UserItemRequest? requestInMemory = allUserItemRequest.Find( irequest => irequest.Guid.Equals(request.Guid) );
            if (requestInMemory is null) return false;
            if (requestInMemory.RequestStatus is not RequestStatus.InProgress) return false;
            requestInMemory.RequestStatus = RequestStatus.Canceled;
            return true;
        }

        public bool CreateAnUser(User user)
        {
            User userInBdd = allUsers.Find( iuser => iuser.Guid == user.Guid);
            if (userInBdd is null)
            {
                allUsers.Add(user);
                return true;
            }
            else return false;
            
        }

        public bool CreateItem(Item item)
        {
            Item itemInMemory = allItems.Find( article => article.Id.Equals(item.Id));
            if (itemInMemory is null)
            {
                allItems.Add(item);
                return true;
            }
            else return false;
        }

        public bool CreateItemUserRequest(UserItemRequest request)
        {
            UserItemRequest requestAlreadyCreated = allUserItemRequest.Find( irequest => irequest.Guid == request.Guid );
            if (requestAlreadyCreated is null)
            {
                allUserItemRequest.Add(request);
                return true;
            }
            else return false;
        }

        public bool DecreseUserPurse(User user, decimal valueToRemove)
        {
            User userInMemory = allUsers.Find(someUser => user.Guid.Equals(someUser.Guid));
            if (userInMemory is not null)
            {
                return userInMemory.UserPurse.RemoveCoins(valueToRemove);
            }
            else return false;
        }

        public bool DeleteAnUser(User user)
        {
            return allUsers.Remove(user);
        }

        public bool DeleteItem(Item item)
        {
            return allItems.Remove(item);
        }

        public bool DeleteItemUserRequest(UserItemRequest request)
        {
            return allUserItemRequest.Remove(request);
        }

        public List<Item> GetAllItem()
        {
            return allItems;
        }

        public List<User> GetAllUser()
        {
            return allUsers;
        }

        public User GetAnUser(Guid id)
        {
            return allUsers.Find( user => user.Guid.Equals(id));
        }

        public Guid? GetGuidUser(string name, string password)
        {
            User? matchingUser = allUsers
                .Where(user => user.UserName.Equals(name))
                  .Where(user => user.UserPassword.Equals(password)).First();
            if (matchingUser is null) return null;
            return matchingUser.Guid;
        }

        public Item? GetItem(Item item)
        {
           return allItems.Find(someArticle => item.Equals(someArticle));
        }

        public Item? GetItem(Guid itemId)
        {
            return allItems.Find(anItem => anItem.Id.Equals(itemId));
        }

        public UserItemRequest GetItemUserRequest(UserItemRequest request)
        {
            return allUserItemRequest.Find(target => target.Guid.Equals(request.Guid));
        }

        public UserItemRequest GetItemUserRequest(Guid guid)
        {
            return allUserItemRequest.Find(someRequest => someRequest.Guid.Equals(guid));
        }

        public List<UserItemRequest> GetListItemUserRequest(RequestStatus status)
        {
            return allUserItemRequest.Where( request => request.RequestStatus.Equals(status)).ToList();
        }

        public List<UserItemRequest> GetListItemUserRequest(User user)
        {
            return allUserItemRequest.Where(request => request.User.Equals(user)).ToList();
        }

        public bool IncreaseUserPurse(User user, decimal valueToAdd)
        {
            User? someUSer = allUsers.Find( someUser => someUser.Guid.Equals(user.Guid));
            if (someUSer is null) return false;
            someUSer.UserPurse.AddCoins(valueToAdd);
            return true;

        }

        public bool UpdateItem(Item item)
        {
            bool existingItem = allItems.Exists( someItem => someItem.Id.Equals(item.Id) );
            if (existingItem is not true) return false;
            bool succefullDelete = allItems.Remove(item);
            allItems.Add(item);
            return succefullDelete;
        }

        public bool UserAccesWithCorrectValue(string name, string password)
        {
            List<User> matchingUser = allUsers
                .Where(user => user.UserName.Equals(name))
                  .Where(user => user.UserPassword.Equals(password)).ToList();
            return matchingUser.Count > 0;
        }

        public bool ValidatedItemUserRequest(UserItemRequest request)
        {
            UserItemRequest? requestInMemory = allUserItemRequest.Find (someRequest => someRequest.Guid.Equals(request.Guid));
            if (requestInMemory is null) return false;
            if (requestInMemory.RequestStatus is RequestStatus.InProgress)
            {
                requestInMemory.RequestStatus = RequestStatus.Validated;
                return true;
            }
            else return false;
        }
    }
}
