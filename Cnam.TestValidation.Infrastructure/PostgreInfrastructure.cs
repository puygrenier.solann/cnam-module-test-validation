﻿using Cnam.TestValidation.Configuration;
using Cnam.TestValidation.Model;
using Dapper;
using Npgsql;

namespace Cnam.TestValidation.Infrastructure
{
    public class PostgreInfrastructure : IDataExchange
    {
        public static string ConnectionString = "Server = ec2-54-77-40-202.eu-west-1.compute.amazonaws.com; Port = 5432; Database = d36r0ue2g175kl; User Id = acedvvwuxdhbdn; Password = 6bce9b7e4c979e436aa30d1fed517d7fed9e3b6b9bedf0094376f0682bddee3a;";
        public PostgreInfrastructure() { }
        public PostgreInfrastructure(string connexion)
        {
            ConnectionString = connexion;
        }
        public bool CreateAnUser(User user)
        {
            string sqlUserInsert = @"INSERT INTO ""Users"" Values (@UserName,@UserPassword,@Id);";
            string sqlPurseInsert = @"INSERT INTO ""UsersPurse"" (""UserName"",""CoinPurse"")  Values (@UserName,0);";
            int affectedRowsUser, affectedRowsPurse;
            try
            {
                using (var connection = new NpgsqlConnection(ConnectionString))
                {
                    affectedRowsUser = connection.Execute(sqlUserInsert, new { UserName = user.UserName, UserPassword = user.UserPassword, Id = user.Guid });
                    affectedRowsPurse = connection.Execute(sqlPurseInsert, new { UserName = user.UserName });
                }
                return (affectedRowsUser > 0) && (affectedRowsPurse > 0);
            }
            catch (PostgresException ex)
            {
                return false;
            }
        }

        public User? GetAnUser(Guid id)
        {
            string sqlSelection = @"SELECT
                                      ""Id"" AS guid,
                                      ""Users"".""UserName"" AS userName,
                                      ""UserPassword"" AS userPassword,
                                      ""UsersPurse"".""CoinPurse"" AS userPurseValue
                                    FROM
                                      ""Users""
                                      INNER JOIN ""UsersPurse"" ON ""UsersPurse"".""UserName"" = ""Users"".""UserName""
                                    WHERE
                                      ""Users"".""Id"" = @Id";
            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                var purse = new UserPurse();
                var affectedRows = connection.Query<User>(sqlSelection, new { Id = id });
                if (affectedRows.Count() > 0) return affectedRows.First();
            }
            return null;
        }

        public bool CreateItem(Item item)
        {
            try
            {
                string sqlInsertion = @"INSERT INTO ""Items"" (""Id"",""Title"",""Price"",""Description"",""EAN"") VALUES (@Id,@Title,@Price,@Description,@EAN)";
                int affectedRows;
                using (var connection = new NpgsqlConnection(ConnectionString))
                {
                    affectedRows = connection.Execute(sqlInsertion, new { Id = item.Id, Title = item.Title, Price = item.Price, Description = item.Description, EAN = item.EAN });
                }
                return (affectedRows > 0);
            }
            catch (PostgresException ex)
            {
                return false;
            }

        }

        public Item? GetItem(Item item)
        {
            string sqlSelect = @"SELECT ""Id"" AS id,""EAN"" AS ean,""Title"" AS title,""Price"" AS price,""Description"" AS description FROM ""Items"" WHERE ""Id""=@Id OR (""EAN""=@EAN AND ""Title""=@Title)";
            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                var affectedRows = connection.Query<Item>(sqlSelect, new { Id = item.Id, EAN = item.EAN, Title = item.Title});
                if (affectedRows.Count() > 0) return affectedRows.First();
            }
            return null;
        }

        public bool DeleteItem(Item item)
        {
            string sqlSelect = @"DELETE FROM ""Items"" WHERE ""EAN""=@EAN";
            int affectedRows;
            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                affectedRows = connection.Execute(sqlSelect, new { EAN = item.EAN });
            }
            return (affectedRows > 0);
        }

        public bool UpdateItem(Item item)
        {
            string sqlSelect = @"UPDATE ""Items"" SET 
                ""Title"" = @Title ,
                ""Price"" = @Price ,
                ""Description"" = @Description,
                ""EAN"" = @EAN
                WHERE ""Id"" = @Id";
            int affectedRows;
            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                affectedRows = connection.Execute(sqlSelect, new { Id = item.Id,Title = item.Title, Price = item.Price, Description = item.Description, EAN = item.EAN });
            }
            return affectedRows > 0;
        }

        public List<Item?> GetAllItem()//id,string ean, string title,decimal price,string description
        {
            string sqlSelect = @"SELECT ""Id"" AS id ,""EAN"" AS ean,""Title"" AS title,""Price"" AS price,""Description"" AS description FROM ""Items""";
            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                var affectedRows = connection.Query<Item?>(sqlSelect);
                if (affectedRows.Count() > 0) return affectedRows.ToList();
            }
            return new List<Item?>();
        }

        public bool DeleteAnUser(User user)
        {
            string sqlQuerryUser = @"DELETE FROM ""Users"" WHERE ""UserName""=@UserName";
            string sqlQuerryPurse = @"DELETE FROM ""UsersPurse"" WHERE ""UserName""=@UserName";
            int affectedRows;
            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                affectedRows = connection.Execute(sqlQuerryUser, new { UserName = user.UserName });
                affectedRows = connection.Execute(sqlQuerryPurse, new { UserName = user.UserName });
            }
            return affectedRows > 0;
        }

        public List<User?> GetAllUser()
        {

            string sqlSelection = @"SELECT
                                      ""Id"" AS guid,
                                      ""Users"".""UserName"" AS userName,
                                      ""UserPassword"" AS userPassword,
                                      ""UsersPurse"".""CoinPurse"" AS userPurseValue
                                    FROM
                                      ""Users""
                                      INNER JOIN ""UsersPurse"" ON ""UsersPurse"".""UserName"" = ""Users"".""UserName""";
            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                var affectedRows = connection.Query<User>(sqlSelection);
                if (affectedRows.Count() > 0) return affectedRows.ToList();
            }
            return new List<User?>();
        }

        public bool IncreaseUserPurse(User user, decimal valueToAdd)
        {
            string sqlString = @"UPDATE ""UsersPurse"" SET ""CoinPurse"" = ""CoinPurse"" + @ValueToAdd WHERE ""UserName""=@UserName";
            int affectedRows;
            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                affectedRows = connection.Execute(sqlString, new { UserName = user.UserName, ValueToAdd = valueToAdd });
            }
            return affectedRows > 0;
        }

        public bool DecreseUserPurse(User user, decimal valueToRemove)
        {
            string sqlString = @"UPDATE ""UsersPurse"" SET ""CoinPurse"" = ""CoinPurse"" - @ValueToAdd WHERE ""UserName""=@UserName";
            int affectedRows;
            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                affectedRows = connection.Execute(sqlString, new { UserName = user.UserName, ValueToAdd = valueToRemove });
            }
            return affectedRows > 0;
        }

        public bool CreateItemUserRequest(UserItemRequest request)
        {
            int affectedRows;
            string sqlQuerry = @"INSERT INTO ""UserRequest"" (""Id"",""UserName"",""RequestedQuantity"",""ItemId"",""RequestStatus"") 
                VALUES(@Id,
                       @UserName,
                       @Quantity,
                       (SELECT ""Id"" FROM ""Items"" WHERE ""Items"".""Id"" = @ItemId),
                       (SELECT ""id"" FROM ""RequestStatus"" WHERE ""RequestStatus"".""id"" = @Status));";
            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                affectedRows = connection.Execute(sqlQuerry, new { Id = request.Guid, UserName = request.User.UserName, Quantity = request.RequestedQuantity, ItemId = request.Item.Id, Status = RequestStatus.InProgress });
            }
            return affectedRows > 0;
        }

        public UserItemRequest? GetItemUserRequest(UserItemRequest request)
        {
            string sqlQuerry = @"SELECT
                                  ""UserRequest"".""Id"" AS Guid,
                                  ""UserRequest"".""RequestedQuantity"",
                                  ""UserRequest"".""RequestStatus"",
                                  ""Users"".""Id"" AS guid,
                                  ""Users"".""UserName"" AS userName,
                                  ""Users"".""UserPassword"",
                                  ""UserRequest"".""ItemId"" AS id ,
                                  ""Items"".""EAN"" AS ean,
                                  ""Items"".""Title"" AS title,
                                  ""Items"".""Price"" AS price,
                                  ""Items"".""Description"" AS description
                                FROM
                                  ""UserRequest""
                                  INNER JOIN ""Items"" ON ""UserRequest"".""ItemId"" = ""Items"".""Id""
                                  INNER JOIN ""Users"" ON ""UserRequest"".""UserName"" = ""Users"".""UserName""
                                WHERE
                                  ""UserRequest"".""Id"" = @Guid";
            //Guid id, User user, Item item, int quantity, RequestStatus status

            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                var returnedRow = connection.Query<UserItemRequest, User, Item, UserItemRequest>(
                    sqlQuerry,
                    (itemRequest, user, item) =>
                    {
                        itemRequest.User = user;
                        itemRequest.Item = item;
                        return itemRequest;
                    },
                    new { Guid = request.Guid },
                    splitOn: "guid,id");
                if (returnedRow.Count() > 0) return returnedRow.First();
            }
            return null;
        }

        public bool CancelItemUserRequest(UserItemRequest request)
        {
            int affectedRows;
            string sqlQuerry = @"UPDATE
                                  ""UserRequest""
                                SET
                                  ""RequestStatus"" = sub_querry.""id""
                                FROM
                                  (
                                    SELECT
                                      ""UserRequest"".""RequestStatus"",
                                      ""RequestStatus"".""id""
                                    FROM
                                      ""RequestStatus""
                                      LEFT JOIN ""UserRequest"" ON ""UserRequest"".""RequestStatus"" = ""RequestStatus"".""id""
                                    WHERE
                                      ""RequestStatus"".""id"" = @RequestStatus
                                  ) AS sub_querry
                                WHERE
                                  ""UserRequest"".""Id"" = @Guid;";
            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                affectedRows = connection.Execute(sqlQuerry, new { Guid = request.Guid, RequestStatus = RequestStatus.Canceled });
            }
            return affectedRows > 0;
        }

        public bool ValidatedItemUserRequest(UserItemRequest request)
        {
            int affectedRows;
            string sqlQuerry = @"UPDATE
                                  ""UserRequest""
                                SET
                                  ""RequestStatus"" = sub_querry.""id""
                                FROM
                                  (
                                    SELECT
                                      ""UserRequest"".""RequestStatus"",
                                      ""RequestStatus"".""id""
                                    FROM
                                      ""RequestStatus""
                                      LEFT JOIN ""UserRequest"" ON ""UserRequest"".""RequestStatus"" = ""RequestStatus"".""id""
                                    WHERE
                                      ""RequestStatus"".""id"" = @RequestStatus
                                  ) AS sub_querry
                                WHERE
                                  ""UserRequest"".""Id"" = @Guid;";
            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                affectedRows = connection.Execute(sqlQuerry, new { Guid = request.Guid, RequestStatus = RequestStatus.Validated });
            }
            return affectedRows > 0;
        }

        public List<UserItemRequest> GetListItemUserRequest(RequestStatus status)
        {
            string sqlQuerry = @"SELECT
                                  ""UserRequest"".""Id"" AS Guid,
                                  ""UserRequest"".""RequestedQuantity"",
                                  ""UserRequest"".""RequestStatus"",
                                  ""Users"".""Id"" AS guid,
                                  ""Users"".""UserName"" AS userName,
                                  ""Users"".""UserPassword"",
                                  ""UserRequest"".""ItemId"" AS id ,
                                  ""Items"".""EAN"" AS ean,
                                  ""Items"".""Title"" AS title,
                                  ""Items"".""Price"" AS price,
                                  ""Items"".""Description"" AS description
                                FROM
                                  ""UserRequest""
                                  INNER JOIN ""Items"" ON ""UserRequest"".""ItemId"" = ""Items"".""Id""
                                  INNER JOIN ""Users"" ON ""UserRequest"".""UserName"" = ""Users"".""UserName""
                                WHERE
                                  ""UserRequest"".""RequestStatus"" = @Status";
            //Guid id, User user, Item item, int quantity, RequestStatus status

            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                var returnedRow = connection.Query<UserItemRequest, User, Item, UserItemRequest>(
                    sqlQuerry,
                    (itemRequest, user, item) =>
                    {
                        itemRequest.User = user;
                        itemRequest.Item = item;
                        return itemRequest;
                    },
                    new { Status = status },
                    splitOn: "guid,id");
                if (returnedRow.Count() > 0) return returnedRow.ToList();
            }
            return null;
        }

        public List<UserItemRequest> GetListItemUserRequest(User user)
        {
            string sqlQuerry = @"SELECT
                                  ""UserRequest"".""Id"" AS Guid,
                                  ""UserRequest"".""RequestedQuantity"",
                                  ""UserRequest"".""RequestStatus"",
                                  ""Users"".""Id"" AS guid,
                                  ""Users"".""UserName"" AS userName,
                                  ""Users"".""UserPassword"",
                                  ""UserRequest"".""ItemId"" AS id ,
                                  ""Items"".""EAN"" AS ean,
                                  ""Items"".""Title"" AS title,
                                  ""Items"".""Price"" AS price,
                                  ""Items"".""Description"" AS description
                                FROM
                                  ""UserRequest""
                                  INNER JOIN ""Items"" ON ""UserRequest"".""ItemId"" = ""Items"".""Id""
                                  INNER JOIN ""Users"" ON ""UserRequest"".""UserName"" = ""Users"".""UserName""
                                WHERE
                                  ""Users"".""Id"" = @Guid";
            //Guid id, User user, Item item, int quantity, RequestStatus status

            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                var returnedRow = connection.Query<UserItemRequest, User, Item, UserItemRequest>(
                    sqlQuerry,
                    (itemRequest, user, item) =>
                    {
                        itemRequest.User = user;
                        itemRequest.Item = item;
                        return itemRequest;
                    },
                    new { Guid = user.Guid },
                    splitOn: "guid,id");
                if (returnedRow.Count() > 0) return returnedRow.ToList<UserItemRequest>();
            }
            return null;
        }

        public UserItemRequest GetItemUserRequest(Guid guid)
        {
            string sqlQuerry = @"SELECT
                                  ""UserRequest"".""Id"" AS Guid,
                                  ""UserRequest"".""RequestedQuantity"",
                                  ""UserRequest"".""RequestStatus"",
                                  ""Users"".""Id"" AS guid,
                                  ""Users"".""UserName"" AS userName,
                                  ""Users"".""UserPassword"",
                                  ""UserRequest"".""ItemId"" AS id ,
                                  ""Items"".""EAN"" AS ean,
                                  ""Items"".""Title"" AS title,
                                  ""Items"".""Price"" AS price,
                                  ""Items"".""Description"" AS description
                                FROM
                                  ""UserRequest""
                                  INNER JOIN ""Items"" ON ""UserRequest"".""ItemId"" = ""Items"".""Id""
                                  INNER JOIN ""Users"" ON ""UserRequest"".""UserName"" = ""Users"".""UserName""
                                WHERE
                                  ""UserRequest"".""Id"" = @Guid";
            //Guid id, User user, Item item, int quantity, RequestStatus status

            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                var returnedRow = connection.Query<UserItemRequest, User, Item, UserItemRequest>(
                    sqlQuerry,
                    (itemRequest, user, item) =>
                    {
                        itemRequest.User = user;
                        itemRequest.Item = item;
                        return itemRequest;
                    },
                    new { Guid = guid },
                    splitOn: "guid,id");
                if (returnedRow.Count() > 0) return returnedRow.First();
            }
            return null;
        }

        public bool DeleteItemUserRequest(UserItemRequest request)
        {
            string sqlRequest = @"DELETE FROM ""UserRequest"" WHERE ""Id"" = @Id";
            int affectedRows;
            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                affectedRows = connection.Execute(sqlRequest, new { Id = request.Guid });
            }
            return affectedRows > 0;
            ;
        }

        public bool UserAccesWithCorrectValue(string name, string password)
        {
            string sqlSelection = @"SELECT
                                      ""Users"".""UserName"" AS userName,
                                      ""UserPassword"" AS userPassword
                                    FROM
                                      ""Users""
                                    WHERE
                                      ""Users"".""UserName"" = @UserName AND
                                      ""Users"".""UserPassword"" = @UserPassword";
            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                var purse = new UserPurse();
                var matchingRow = connection.Query<User>(sqlSelection, new { UserName = name, UserPassword = password });
                return matchingRow.Count() > 0;
            }
        }

        public Guid? GetGuidUser(string name, string password)
        {
            string sqlSelection = @"SELECT
                                      ""Users"".""Id"" 

                                      FROM ""Users""
                                    WHERE
                                      ""Users"".""UserName"" = @UserName AND
                                      ""Users"".""UserPassword"" = @UserPassword";
            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                var purse = new UserPurse();
                Guid? guidUser = connection.Query<Guid>(sqlSelection, new { UserName = name, UserPassword = password }).First();
                return guidUser;
            }
        }

        public Item? GetItem(Guid itemId)
        {
            string sqlSelect = @"SELECT ""Id"" AS id,""EAN"" AS ean,""Title"" AS title,""Price"" AS price,""Description"" AS description FROM ""Items"" WHERE ""Id""=@Id";
            using (var connection = new NpgsqlConnection(ConnectionString))
            {
                var affectedRows = connection.Query<Item>(sqlSelect, new { Id = itemId });
                if (affectedRows.Count() > 0) return affectedRows.First();
            }
            return null;
        }
    }
}