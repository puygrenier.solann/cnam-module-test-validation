﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cnam.TestValidation.DomainService.CofeeUseCaseException
{
    public class CofeeUseCaseUserCreationException : GenericUseCaseException
    {
        public CofeeUseCaseUserCreationException(string? message) : base(message)
        {
        }
    }
}
