﻿using Cnam.TestValidation.Infrastructure;
using Cnam.TestValidation.Model;

using Cnam.TestValidation.DomainService.CofeeUseCaseException;

namespace Cnam.TestValidation.DomainService
{
    public class CofeeUseCase
    {
       // private UserItemRequestUpdateur userItemRequestUpdater;
        private IDataExchange dataLayer;

        public CofeeUseCase(IDataExchange dataLayer)
        {
            this.dataLayer = dataLayer;
            //userItemRequestUpdater = new();
        }

        public void AddUser(User user)
        {
            bool succesfullAddedUser = dataLayer.CreateAnUser(user); //allUsers.AddUser(user);
            if (succesfullAddedUser is not true) throw new CofeeUseCaseUserCreationException("User already exist");
        }
        public void AddUser(string name, string password)
        {
            AddUser(new User(name, password));
        }

        public Guid ProvideGuidOfUser(string name, string password)
        {
            bool succesfullAcces = dataLayer.UserAccesWithCorrectValue(name, password);
            if (succesfullAcces is not true) throw new CofeeUseCaseUserErrorUserAuthenthificationException("User doesn't exist");
            Guid? guidResult = dataLayer.GetGuidUser(name,password);
            if (guidResult is null) throw new CofeeUseCaseUserAccesException("User doesn't exist");
            else return (Guid)guidResult;
        }
        public User GetUser(Guid id)
        {
            User? result = dataLayer.GetAnUser(id);
            if (result is null) throw new CofeeUseCaseUserAccesException("User doesn't exist");
            else return result;
        }
        public User GetUser(User user)
        {
            return GetUser(user.Guid);
        }

        public List<User> GellAllUser()
        {
            return dataLayer.GetAllUser();
        }

        public void DeleteUser(User user)
        {
            User? result = dataLayer.GetAnUser(user.Guid);
            if (result is null) throw new GenericUseCaseException("User doesn't exist");
            dataLayer.DeleteAnUser(user);
        }

        public void ActionOnPurse(PurseAction action, User user, decimal purseValue)
        {
            if (purseValue < 0 ) throw new CofeeUseCaseValueException("purseValue cannot be use with a negativ number");
            switch (action)
            {
                case PurseAction.Add:
                    dataLayer.IncreaseUserPurse(user, purseValue);
                    break;
                case PurseAction.Remove:
                    User userFromData = dataLayer.GetAnUser(user.Guid);
                    if(userFromData.UserPurse.CoinPurse < purseValue) throw new CofeeUseCaseUserPurseUpdateException("User cannot have negativ purse value");
                    dataLayer.DecreseUserPurse(user, purseValue); 
                    break;
                default: throw new ArgumentException("Action on purse is unknow");
            }
        }

        public void AddItem(Item item)
        {
            Item? existingItem = dataLayer.GetItem(item);
            if (existingItem is not null) throw new CofeeUseCaseItemCreationException("Item already exsting and cannot be added");
            bool succefullAddedItem = dataLayer.CreateItem(item);
            if (succefullAddedItem is not true) throw new CofeeUseCaseItemCreationException("Item cannot be added into bdd");
        }

        public void DeleteItem(Item item)
        {
            bool succefullDeletedItem = dataLayer.DeleteItem(item);
            if (succefullDeletedItem is not true) throw new GenericUseCaseException("Item cannot be deleted");
        }

        public Item GetItem(Item item)
        {
            Item? resultItem = dataLayer.GetItem(item);
            if (resultItem is null) throw new CofeeUseCaseItemAccesException("Item doesn't exist");
            else return resultItem;
        }
        public Item GetItem(Guid item)
        {
            Item? resultItem = dataLayer.GetItem(item);
            if (resultItem is null) throw new CofeeUseCaseItemAccesException("Item doesn't exist");
            else return resultItem;
        }


        public void UpdateItem(Item itemToUpdate)
        {
            bool succefullUpdateItem = dataLayer.UpdateItem(itemToUpdate);
            if (succefullUpdateItem is not true) throw new CofeeUseCaseItemAccesException("Item doesn't exist");
        }

        public List<Item> GetAllItem()
        {
            return dataLayer.GetAllItem();
        }

        public Guid CreateRequestItemOfUser(User userAsking, Item itemRequested, int quantityRequested)
        {
            if (IsExistingItem(itemRequested) is not true) throw new CofeeUseCaseItemAccesException("Item doesn't exist");
            if (IsExistingUser(userAsking) is not true) throw new CofeeUseCaseUserAccesException("User doesn't exist");
            if (quantityRequested is < 0) throw new ArgumentOutOfRangeException("Request cannot be created: quantity requested cannot be negativ");
            UserItemRequest? request = CreateCreditingRequest(userAsking, itemRequested, quantityRequested);
            if (request is null) throw new CofeeUseCaseRequestCreationException("Request cannot be created : not enougth money");
            else return request.Guid;
        }

        public List<UserItemRequest> GetRequestWith(RequestStatus progressStatus)
        {
            return dataLayer.GetListItemUserRequest(progressStatus);
        }

        public List<UserItemRequest> GetRequestWith(User user)
        {
            return dataLayer.GetListItemUserRequest(user);
        }
        public List<UserItemRequest> GetRequestWith(User user,RequestStatus status)
        {
            return GetRequestWith(user).Where(request => request.RequestStatus.Equals(status)).ToList(); //dataLayer.GetListItemUserRequest(user);
        }

        public UserItemRequest GetRequestWith(Guid requestId)
        {
            UserItemRequest? request = dataLayer.GetItemUserRequest(requestId);
            if (request is null) throw new CofeeUseCaseRequestAccesExeption("Request ID unknow");
            else return request;
        }
        public UserItemRequest GetRequestWith(UserItemRequest request)
        {
            return GetRequestWith(request.Guid);
        }

        public void ValidateRequest(UserItemRequest request)
        {
            UserItemRequest? result = dataLayer.GetItemUserRequest(request.Guid);
            if (result is null) throw new CofeeUseCaseRequestAccesExeption("Request cannot be validated: it doesn't exist");
            if (result.RequestStatus is RequestStatus.Validated) throw new CofeeUseCaseRequestUpdateException("Request already validated");
            if (result.RequestStatus is RequestStatus.Canceled) throw new CofeeUseCaseRequestUpdateException("Request is in state canceled");
            bool succesfullUpdate = dataLayer.ValidatedItemUserRequest(result);
            if (succesfullUpdate is not true) throw new CofeeUseCaseUserPurseUpdateException("Request cannot be validated in Database");
        }

        public void CancelRequest(UserItemRequest request)
        {
            UserItemRequest? result = dataLayer.GetItemUserRequest(request.Guid);
            if (result is null) throw new CofeeUseCaseRequestAccesExeption("Request cannot be canceled: it doesn't exist");
            if (result.RequestStatus is RequestStatus.Validated) throw new CofeeUseCaseRequestUpdateException("Requestis in state validated");
            if (result.RequestStatus is RequestStatus.Canceled) throw new CofeeUseCaseRequestUpdateException("Request already canceled");
            bool succesfullUpdate = dataLayer.CancelItemUserRequest(request);
            if (succesfullUpdate is not true) throw new CofeeUseCaseRequestUpdateException("Request cannot be canceled in Database");

            bool succefullRefund = dataLayer.IncreaseUserPurse(request.User, request.GetCostOfRequest());
            if (succefullRefund is not true) throw new CofeeUseCaseUserPurseUpdateException("Request status cancel does not refund user");
        }

        private UserItemRequest? CreateCreditingRequest(User userAsking, Item itemRequested, int quantityRequested)
        {
            if (userAsking is null) throw new CofeeUseCaseUserAccesException("Request cannot be created: userAsking is null");
            if (itemRequested is null) throw new CofeeUseCaseItemAccesException("Request cannot be created: itemRequested is null");

            UserItemRequest itemRequest = new UserItemRequest(userAsking, itemRequested, quantityRequested); ;
            if (userAsking.CanBuy(itemRequest.GetCostOfRequest()) is false) return null;
            
            bool succesfullCreation = dataLayer.CreateItemUserRequest(itemRequest);
            if (succesfullCreation is false)
            {
                throw new CofeeUseCaseRequestCreationException("Request cannot be created into data base");
            }
            bool succefullUpdate = dataLayer.DecreseUserPurse(userAsking, itemRequest.GetCostOfRequest());
            if (succefullUpdate is false)
            {
                dataLayer.DeleteItemUserRequest(itemRequest);
                throw new CofeeUseCaseUserPurseUpdateException("Cannot decrease user purse value") ;
            }
            return dataLayer.GetItemUserRequest(itemRequest);
        }

        private bool IsExistingItem(Item? item)
        {
            if (item is null) return false;
            Item? itemToCheck = GetItem(item);
            return itemToCheck is not null;
        }
        private bool IsExistingUser(User? user)
        {
            if (user is null) return false;
            User? userToCheck = GetUser(user);
            return userToCheck is not null;
        }
    }
}
