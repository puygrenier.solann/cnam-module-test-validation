﻿using Cnam.TestValidation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cnam.TestValidation.DomainService
{
    public interface IMediatorUseCase
    {
        public void NotifyMediatorForInsertUserData(User createdUser);
    }
}
