﻿using Cnam.TestValidation.Infrastructure;
using Cnam.TestValidation.Model;
using Dapper;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Cnam.TestValidation.Testing
{
    [Trait("Category", "Infra: Connection")]
    public class InfrastructureTest
    {
        public static string connectionStringForTestBdd = "Server = ec2-54-77-40-202.eu-west-1.compute.amazonaws.com; Port = 5432; Database = d36r0ue2g175kl; User Id = acedvvwuxdhbdn; Password = 6bce9b7e4c979e436aa30d1fed517d7fed9e3b6b9bedf0094376f0682bddee3a;";

        private readonly ITestOutputHelper output;
        public IDataExchange infra = new PostgreInfrastructure(connectionStringForTestBdd);
        public InfrastructureTest(ITestOutputHelper outputHelper)
        {
            output = outputHelper;
        }

        [Fact(DisplayName = "Can Connect To Dev Database Environement")]
        public void CanConnectToDevDatabase()
        {
            using (var connection = new NpgsqlConnection(connectionStringForTestBdd))
            {
                connection.Open();
            }
        }

        [Fact(DisplayName = "Can Perform Create,Delete Or Read Action On User")]
        public void CanPerformCreateOrReadActionOnUser()
        {
            
            User user = TestFacilitator.RandomUserGenerator();
            bool isSuccefullChange;
            //CREATE
            isSuccefullChange = infra.CreateAnUser(user);
            Assert.True(isSuccefullChange);
            //READ
            User userFromInfra = infra.GetAnUser(user.Guid);
            Assert.Equal(user, userFromInfra);
            //DELETE
            isSuccefullChange = infra.DeleteAnUser(user);
            Assert.True(isSuccefullChange);
            userFromInfra = infra.GetAnUser(user.Guid);
            Assert.Null(userFromInfra);
        }

        [Fact(DisplayName = "Can Perform CRUD Action On Item")]
        public void CanPerformCRUDActionOnItem()
        {
            
            Item item = TestFacilitator.RandomItemGenerator();
            bool isSuccefullChange;
            //CREATE
            isSuccefullChange = infra.CreateItem(item);
            Assert.True(isSuccefullChange);
            //READ
            Item itemFromBDD = infra.GetItem(item);
            Assert.Equal(item, itemFromBDD);
            //UPDATE
            Item itemUpdated = new Item { 
                Id = itemFromBDD.Id,
                Price= TestFacilitator.ProvideRandomPrice(), 
                Title = itemFromBDD .Title, 
                Description = itemFromBDD.Description,
                EAN = itemFromBDD.EAN
            };
            isSuccefullChange = infra.UpdateItem(itemUpdated);
            Assert.True(isSuccefullChange);
            itemFromBDD = infra.GetItem(itemUpdated);
            Assert.Equal(itemUpdated, itemFromBDD);
            //DELETE
            isSuccefullChange = infra.DeleteItem(itemUpdated);
            Assert.True(isSuccefullChange);
            Item? itemNullBecauseDeleteFromBDD = infra.GetItem(item);
            Assert.Null(itemNullBecauseDeleteFromBDD);
        }

        [Fact(DisplayName = "Get All Items")]
        public void GetAllItems()
        {
            List<Item> allItems = infra.GetAllItem();

            Item itemA = TestFacilitator.RandomItemGenerator();
            Item itemB = TestFacilitator.RandomItemGenerator();
            
            allItems.Add(itemA);
            allItems.Add(itemB);
            infra.CreateItem(itemA);
            infra.CreateItem(itemB);

            List<Item> allItemsFromBdd = infra.GetAllItem();

            Assert.NotNull(allItems);
            Assert.NotNull(allItemsFromBdd);
            foreach (Item item in allItems)
            {
                Assert.Contains(item, allItemsFromBdd);
            }
            //Clean the test
            infra.DeleteItem(itemA);
            infra.DeleteItem(itemB);
        }

        [Fact(DisplayName = "Get All Users")]
        public void GetAllUsers()
        {
            
            List<User> allUsers = infra.GetAllUser();

            User userA = TestFacilitator.RandomUserGenerator();
            User userB = TestFacilitator.RandomUserGenerator();

            allUsers.Add(userA);
            allUsers.Add(userB);
            infra.CreateAnUser(userA);
            infra.CreateAnUser(userB);

            List<User>  allUserFromDataLayer = infra.GetAllUser();
            foreach (User user in allUsers)
            {
                Assert.Contains(user, allUserFromDataLayer);
            }
            
            //clean the test
            infra.DeleteAnUser(userA);
            infra.DeleteAnUser(userB);
        }

        [Fact(DisplayName = "Can Update Value Into Purse")]
        public void CanUpdateValueIntoPurse()
        {
            int initialValue=10;
            int valueToDecrease = 3;


            User user = TestFacilitator.RandomUserGenerator();
            infra.CreateAnUser(user);

            bool successfullUpdate = infra.IncreaseUserPurse(user, initialValue);
            Assert.True(successfullUpdate);
            Assert.Equal(initialValue, infra.GetAnUser(user.Guid).UserPurse.CoinPurse);

            successfullUpdate = infra.DecreseUserPurse(user, valueToDecrease);
            Assert.True(successfullUpdate);
            Assert.Equal(initialValue - valueToDecrease, infra.GetAnUser(user.Guid).UserPurse.CoinPurse);
            //Clean Test
            infra.DeleteAnUser(user);
        }
            
            [Fact(DisplayName = "Can Perfrom Create, Read and Delete Action On A Request")]
            public void CanPerfromCreateandReadAndDeleteActionOnARequest()
            {
                
                User user = TestFacilitator.RandomUserGenerator();
                Item item = TestFacilitator.RandomItemGenerator();

                infra.CreateItem(item);
                infra.CreateAnUser(user);
                infra.IncreaseUserPurse(user, TestFacilitator.ProvideRandomInt());
                UserItemRequest userItemRequest = new UserItemRequest(user, item, 2);
                
                bool succesfullCreation = infra.CreateItemUserRequest(userItemRequest);
                Assert.True(succesfullCreation);

                UserItemRequest userItemRequestFromBdd = infra.GetItemUserRequest(userItemRequest.Guid);
                Assert.Equal(userItemRequestFromBdd, userItemRequest);

                userItemRequestFromBdd = infra.GetItemUserRequest(userItemRequest);
                Assert.Equal(userItemRequestFromBdd, userItemRequest);

                userItemRequestFromBdd = infra.GetListItemUserRequest(userItemRequest.User).First();
                Assert.Equal(userItemRequestFromBdd, userItemRequest);

                userItemRequestFromBdd = infra.GetListItemUserRequest(RequestStatus.InProgress).Find( request => request.Guid.Equals(userItemRequest.Guid));
                Assert.Equal(userItemRequestFromBdd, userItemRequest);

                bool successfulDelete = infra.DeleteItemUserRequest(userItemRequest);
                Assert.True(successfulDelete);
                userItemRequestFromBdd = infra.GetItemUserRequest(userItemRequest);
                Assert.Null(userItemRequestFromBdd);

                //Clean Test
                infra.DeleteItem(item);
                infra.DeleteAnUser(user);
        }

        [Fact(DisplayName = "Can Get All Request By User Or Status")]
        public void CanGetAllRequestByUserOrStatus()
        {
            
            User user = TestFacilitator.RandomUserGenerator();
            Item item = TestFacilitator.RandomItemGenerator();

            infra.CreateItem(item);
            infra.CreateAnUser(user);
            UserItemRequest userItemRequest = new UserItemRequest(user, item, 1);
            bool succesfullCreation = infra.CreateItemUserRequest(userItemRequest);
            Assert.True(succesfullCreation);

            List<UserItemRequest> userItemRequestFromBdd = infra.GetListItemUserRequest(RequestStatus.InProgress);
            Assert.Contains(userItemRequest, userItemRequestFromBdd);

            userItemRequestFromBdd.Clear();
            userItemRequestFromBdd = infra.GetListItemUserRequest(user);
            if (userItemRequestFromBdd is null) output.WriteLine("null...");
            else output.WriteLine(userItemRequestFromBdd.Count.ToString());
            Assert.Contains(userItemRequest, userItemRequestFromBdd);

            //Clean Test
            infra.DeleteItem(item);
            infra.DeleteAnUser(user);
        }

        [Fact(DisplayName = "Can Cancel Status On A Request")]
        public void CanCancelStatusOnARequest()
        {
            
            User user = TestFacilitator.RandomUserGenerator();
            Item item = TestFacilitator.RandomItemGenerator();
            infra.CreateItem(item);
            infra.CreateAnUser(user);
            UserItemRequest userItemRequest = new UserItemRequest(user, item, 2);
            bool succesfullCreation = infra.CreateItemUserRequest(userItemRequest);

            bool succefullCancelation = infra.CancelItemUserRequest(userItemRequest);
            Assert.True(succefullCancelation);
            UserItemRequest userItemRequestFromBdd = infra.GetItemUserRequest(userItemRequest);
            Assert.True(userItemRequestFromBdd.RequestStatus.Equals(RequestStatus.Canceled));

        }

        [Fact(DisplayName = "Can Validate Status On A Request")]
        public void CanValidateStatusOnARequest()
        {
            
            User user = TestFacilitator.RandomUserGenerator();
            Item item = TestFacilitator.RandomItemGenerator();
            infra.CreateItem(item);
            infra.CreateAnUser(user);
            UserItemRequest userItemRequest = new UserItemRequest(user, item, 2);
            bool succesfullCreation = infra.CreateItemUserRequest(userItemRequest);

            bool succefullCancelation = infra.ValidatedItemUserRequest(userItemRequest);

            UserItemRequest userItemRequestFromBdd = infra.GetItemUserRequest(userItemRequest);
            Assert.True(userItemRequestFromBdd.RequestStatus.Equals(RequestStatus.Validated));

        }
    }
}
