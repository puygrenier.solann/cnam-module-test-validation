﻿using Cnam.TestValidation.Model;
using Cnam.TestValidation.WebApi.WebServiceBody;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Cnam.TestValidation.Testing.Integration
{
    /// <summary>
    /// Class containing the tests of the correct calls of the API
    /// Can be call using the command line. For more information see dotnet test -h
    /// </summary>
    /// <example>Test can be run using bash : dotnet test --filter "Category=Integration: Request"</example>
    [Trait("Category", "Integration: Request")]
    public class WebApiRequestControllerIntegrationTest
    {
        private readonly ITestOutputHelper output;
        public WebApiRequestControllerIntegrationTest(ITestOutputHelper outputHelper)
        {
            output = outputHelper;
        }

        [Fact(DisplayName = "POST: Return 201 when creating crediting request")]
        public async Task POST_Provide201whencreatingcreditingrequest()
        {
            User randomUser = TestFacilitator.RandomUserGenerator();
            UserPostBodyRequest body = new UserPostBodyRequest { UserName = randomUser.UserName, Password = randomUser.UserPassword };
            HttpResponseMessage response;
            Guid userId;
            Guid itemId;
            Item randomItem = TestFacilitator.RandomItemGenerator();
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
                    response = await httpClient.SendAsync(request);
                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.True(System.Net.HttpStatusCode.Created.Equals(response.StatusCode));
                }
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user/token"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
                    response = await httpClient.SendAsync(request);
                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.True(System.Net.HttpStatusCode.OK.Equals(response.StatusCode));
                    bool succefullGuid = Guid.TryParse(response.Content.ReadAsStringAsync().Result.Replace("\"", ""), out userId);
                    Assert.True(succefullGuid);
                }
                using (var request = new HttpRequestMessage(new HttpMethod("PUT"), $"https://localhost:7210/api/CofeeManagerUser/user/{userId}/purse/add/{randomItem.Price}"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");

                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.OK, response.StatusCode);
                }
                using (var request = new HttpRequestMessage(new HttpMethod("POST"), "https://localhost:7210/api/CofeeManagerItems/item"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(randomItem.ToString(), Encoding.UTF8, "application/json");
                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body :\n" + request.Content.ReadAsStringAsync().Result);
                    response = await httpClient.SendAsync(request);
                    output.WriteLine("Response :\n" + response.ToString());
                    string jsonResponse = response.Content.ReadAsStringAsync().Result;
                    output.WriteLine("Response body:\n" + jsonResponse);
                    Assert.True(response.StatusCode is System.Net.HttpStatusCode.Created);
                    Assert.Contains("SUCCES", jsonResponse);
                }
                using (var request = new HttpRequestMessage(new HttpMethod("GET"), "https://localhost:7210/api/CofeeManagerItems/item/all"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);
                    var allItems = response.Content.ReadAsStringAsync().Result;
                    Item[]? deserializedObject = JsonConvert.DeserializeObject<Item[]?>(response.Content.ReadAsStringAsync().Result);
                    itemId = deserializedObject.Where(item => item.Equals(randomItem)).First().Id;
                }
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerRequest/request"))
                {
                    RequestPostBodyRequest bodyForRequest = new RequestPostBodyRequest { UserId = userId, RequestedItem = itemId, Quantity = 1 };
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(bodyForRequest), Encoding.UTF8, "application/json");
                    response = await httpClient.SendAsync(request);
                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.Created,response.StatusCode);
                }
            }
        }

        [Fact(DisplayName = "POST: Return 404 When Using Inexistant User or Item")]
        public async Task POST_Code404WhenUsingInexistantUserorItem()
        {
            User randomUser = TestFacilitator.RandomUserGenerator();
            UserPostBodyRequest body = new UserPostBodyRequest { UserName = randomUser.UserName, Password = randomUser.UserPassword };
            HttpResponseMessage response;
            Guid userId;
            Guid itemId;
            Guid falseUserId = Guid.NewGuid();
            Guid falseItemId = Guid.NewGuid();
            Item randomItem = TestFacilitator.RandomItemGenerator();
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
                    response = await httpClient.SendAsync(request);
                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.True(System.Net.HttpStatusCode.Created.Equals(response.StatusCode));
                }
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user/token"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
                    response = await httpClient.SendAsync(request);
                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.True(System.Net.HttpStatusCode.OK.Equals(response.StatusCode));
                    bool succefullGuid = Guid.TryParse(response.Content.ReadAsStringAsync().Result.Replace("\"", ""), out userId);
                    Assert.True(succefullGuid);
                }
                using (var request = new HttpRequestMessage(new HttpMethod("PUT"), $"https://localhost:7210/api/CofeeManagerUser/user/{userId}/purse/add/{randomItem.Price}"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");

                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.OK, response.StatusCode);
                }
                using (var request = new HttpRequestMessage(new HttpMethod("POST"), "https://localhost:7210/api/CofeeManagerItems/item"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(randomItem.ToString(), Encoding.UTF8, "application/json");
                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body :\n" + request.Content.ReadAsStringAsync().Result);
                    response = await httpClient.SendAsync(request);
                    output.WriteLine("Response :\n" + response.ToString());
                    string jsonResponse = response.Content.ReadAsStringAsync().Result;
                    output.WriteLine("Response body:\n" + jsonResponse);
                    Assert.True(response.StatusCode is System.Net.HttpStatusCode.Created);
                    Assert.Contains("SUCCES", jsonResponse);
                }
                using (var request = new HttpRequestMessage(new HttpMethod("GET"), "https://localhost:7210/api/CofeeManagerItems/item/all"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);
                    var allItems = response.Content.ReadAsStringAsync().Result;
                    Item[]? deserializedObject = JsonConvert.DeserializeObject<Item[]?>(response.Content.ReadAsStringAsync().Result);
                    itemId = deserializedObject.Where(item => item.Equals(randomItem)).First().Id;
                }
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerRequest/request"))
                {
                    RequestPostBodyRequest bodyForRequest = new RequestPostBodyRequest { UserId = userId, RequestedItem = falseItemId, Quantity = 1 };
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(bodyForRequest), Encoding.UTF8, "application/json");
                    response = await httpClient.SendAsync(request);
                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.NotFound, response.StatusCode);
                }
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerRequest/request"))
                {
                    RequestPostBodyRequest bodyForRequest = new RequestPostBodyRequest { UserId = falseUserId, RequestedItem = itemId, Quantity = 1 };
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(bodyForRequest), Encoding.UTF8, "application/json");
                    response = await httpClient.SendAsync(request);
                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.NotFound, response.StatusCode);
                }
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerRequest/request"))
                {
                    RequestPostBodyRequest bodyForRequest = new RequestPostBodyRequest { UserId = falseUserId, RequestedItem = falseItemId, Quantity = 1 };
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(bodyForRequest), Encoding.UTF8, "application/json");
                    response = await httpClient.SendAsync(request);
                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.NotFound, response.StatusCode);
                }
            }
        }

        [Fact(DisplayName = "POST: Return 400 When Creating Crediting Request With Error")]
        public async Task POST_Return400WhenCreatingCreditingRequestWithError()
        {
            User randomUser = TestFacilitator.RandomUserGenerator();
            UserPostBodyRequest body = new UserPostBodyRequest { UserName = randomUser.UserName, Password = randomUser.UserPassword };
            HttpResponseMessage response;
            Guid userId;
            Guid itemId;
            Item randomItem = TestFacilitator.RandomItemGenerator();
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
                    response = await httpClient.SendAsync(request);
                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.True(System.Net.HttpStatusCode.Created.Equals(response.StatusCode));
                }
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user/token"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
                    response = await httpClient.SendAsync(request);
                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.True(System.Net.HttpStatusCode.OK.Equals(response.StatusCode));
                    bool succefullGuid = Guid.TryParse(response.Content.ReadAsStringAsync().Result.Replace("\"", ""), out userId);
                    Assert.True(succefullGuid);
                }
                using (var request = new HttpRequestMessage(new HttpMethod("PUT"), $"https://localhost:7210/api/CofeeManagerUser/user/{userId}/purse/add/{randomItem.Price}"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");

                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.OK, response.StatusCode);
                }
                using (var request = new HttpRequestMessage(new HttpMethod("POST"), "https://localhost:7210/api/CofeeManagerItems/item"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(randomItem.ToString(), Encoding.UTF8, "application/json");
                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body :\n" + request.Content.ReadAsStringAsync().Result);
                    response = await httpClient.SendAsync(request);
                    output.WriteLine("Response :\n" + response.ToString());
                    string jsonResponse = response.Content.ReadAsStringAsync().Result;
                    output.WriteLine("Response body:\n" + jsonResponse);
                    Assert.True(response.StatusCode is System.Net.HttpStatusCode.Created);
                    Assert.Contains("SUCCES", jsonResponse);
                }
                using (var request = new HttpRequestMessage(new HttpMethod("GET"), "https://localhost:7210/api/CofeeManagerItems/item/all"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);
                    var allItems = response.Content.ReadAsStringAsync().Result;
                    Item[]? deserializedObject = JsonConvert.DeserializeObject<Item[]?>(response.Content.ReadAsStringAsync().Result);
                    itemId = deserializedObject.Where(item => item.Equals(randomItem)).First().Id;
                }
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerRequest/request"))
                {
                    RequestPostBodyRequest bodyForRequest = new RequestPostBodyRequest { UserId = userId, RequestedItem = itemId, Quantity = -1 };
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(bodyForRequest), Encoding.UTF8, "application/json");
                    response = await httpClient.SendAsync(request);
                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.BadRequest, response.StatusCode);
                }
            }
        }

        [Fact(DisplayName = "POST: Return 409 when creating crediting request")]
        public async Task POST_Provide409whencreatingcreditingrequest()
        {
            User randomUser = TestFacilitator.RandomUserGenerator();
            UserPostBodyRequest body = new UserPostBodyRequest { UserName = randomUser.UserName, Password = randomUser.UserPassword };
            HttpResponseMessage response;
            Guid userId;
            Guid itemId;
            Item randomItem = TestFacilitator.RandomItemGenerator();
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
                    response = await httpClient.SendAsync(request);
                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.True(System.Net.HttpStatusCode.Created.Equals(response.StatusCode));
                }
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user/token"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
                    response = await httpClient.SendAsync(request);
                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.True(System.Net.HttpStatusCode.OK.Equals(response.StatusCode));
                    bool succefullGuid = Guid.TryParse(response.Content.ReadAsStringAsync().Result.Replace("\"", ""), out userId);
                    Assert.True(succefullGuid);
                }
                using (var request = new HttpRequestMessage(new HttpMethod("POST"), "https://localhost:7210/api/CofeeManagerItems/item"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(randomItem.ToString(), Encoding.UTF8, "application/json");
                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body :\n" + request.Content.ReadAsStringAsync().Result);
                    response = await httpClient.SendAsync(request);
                    output.WriteLine("Response :\n" + response.ToString());
                    string jsonResponse = response.Content.ReadAsStringAsync().Result;
                    output.WriteLine("Response body:\n" + jsonResponse);
                    Assert.True(response.StatusCode is System.Net.HttpStatusCode.Created);
                    Assert.Contains("SUCCES", jsonResponse);
                }
                using (var request = new HttpRequestMessage(new HttpMethod("GET"), "https://localhost:7210/api/CofeeManagerItems/item/all"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);
                    var allItems = response.Content.ReadAsStringAsync().Result;
                    Item[]? deserializedObject = JsonConvert.DeserializeObject<Item[]?>(response.Content.ReadAsStringAsync().Result);
                    itemId = deserializedObject.Where(item => item.Equals(randomItem)).First().Id;
                }
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerRequest/request"))
                {
                    RequestPostBodyRequest bodyForRequest = new RequestPostBodyRequest { UserId = userId, RequestedItem = itemId, Quantity = 1 };
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(bodyForRequest), Encoding.UTF8, "application/json");
                    response = await httpClient.SendAsync(request);
                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.Conflict, response.StatusCode);
                }
            }
        }

        [Fact(DisplayName = "PUT: Return 200 When Succefull Update")]
        public async Task PUT_Return200WhenSuccefullUpdate()
        {
            User randomUser = TestFacilitator.RandomUserGenerator();
            UserPostBodyRequest body = new UserPostBodyRequest { UserName = randomUser.UserName, Password = randomUser.UserPassword };
            HttpResponseMessage response;
            Guid userId;
            Guid itemId;
            Item randomItem = TestFacilitator.RandomItemGenerator();
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
                    response = await httpClient.SendAsync(request);
                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.True(System.Net.HttpStatusCode.Created.Equals(response.StatusCode));
                }
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user/token"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
                    response = await httpClient.SendAsync(request);
                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.True(System.Net.HttpStatusCode.OK.Equals(response.StatusCode));
                    bool succefullGuid = Guid.TryParse(response.Content.ReadAsStringAsync().Result.Replace("\"", ""), out userId);
                    Assert.True(succefullGuid);
                }
                using (var request = new HttpRequestMessage(new HttpMethod("POST"), "https://localhost:7210/api/CofeeManagerItems/item"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(randomItem.ToString(), Encoding.UTF8, "application/json");
                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body :\n" + request.Content.ReadAsStringAsync().Result);
                    response = await httpClient.SendAsync(request);
                    output.WriteLine("Response :\n" + response.ToString());
                    string jsonResponse = response.Content.ReadAsStringAsync().Result;
                    output.WriteLine("Response body:\n" + jsonResponse);
                    Assert.True(response.StatusCode is System.Net.HttpStatusCode.Created);
                    Assert.Contains("SUCCES", jsonResponse);
                }
                using (var request = new HttpRequestMessage(new HttpMethod("GET"), "https://localhost:7210/api/CofeeManagerItems/item/all"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);
                    var allItems = response.Content.ReadAsStringAsync().Result;
                    Item[]? deserializedObject = JsonConvert.DeserializeObject<Item[]?>(response.Content.ReadAsStringAsync().Result);
                    itemId = deserializedObject.Where(item => item.Equals(randomItem)).First().Id;
                }
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerRequest/request"))
                {
                    RequestPostBodyRequest bodyForRequest = new RequestPostBodyRequest { UserId = userId, RequestedItem = itemId, Quantity = 1 };
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(bodyForRequest), Encoding.UTF8, "application/json");
                    response = await httpClient.SendAsync(request);
                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.Conflict, response.StatusCode);
                }
            }
        }
    }
}
