﻿using Cnam.TestValidation.Model;
using Cnam.TestValidation.WebApi.WebServiceBody;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Cnam.TestValidation.Testing.Integration
{
    /// <summary>
    /// Class containing the tests of the correct calls of the API
    /// Can be call using the command line. For more information see dotnet test -h
    /// </summary>
    /// <example>Test can be run using bash : dotnet test --filter "Category=Integration: User Controller"</example>
    [Trait("Category", "Integration: User Controller")]
    public class WebApiUserControllerIntegrationTest
    {
        private readonly ITestOutputHelper output;
        public WebApiUserControllerIntegrationTest(ITestOutputHelper outputHelper)
        {
            output = outputHelper;
        }

        [Fact(DisplayName = "POST: Return 201 When Succesfull Creation Of User")]
        public async Task POST_Return201WhenSuccesfullCreationOfUser()
        {
            User randomUser = TestFacilitator.RandomUserGenerator();
            UserPostBodyRequest body = new UserPostBodyRequest { UserName = randomUser.UserName, Password = randomUser.UserPassword };
            HttpResponseMessage response;
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");

                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.Created, response.StatusCode);
                }
            }
        }

        [Fact(DisplayName = "POST: Return 409 When Creation Of User Twice")]
        public async Task POST_Return409WhenCreationOfUserTwice()
        {
            User randomUser = TestFacilitator.RandomUserGenerator();
            UserPostBodyRequest body = new UserPostBodyRequest { UserName = randomUser.UserName, Password = randomUser.UserPassword };
            HttpResponseMessage response;
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(body.ToString(), Encoding.UTF8, "application/json");

                    response = await httpClient.SendAsync(request);

                    Assert.True(System.Net.HttpStatusCode.Created.Equals(response.StatusCode));
                }

                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(body.ToString(), Encoding.UTF8, "application/json");

                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.Conflict, response.StatusCode);
                }
            }
        }

        [Fact(DisplayName = "POST: Return 400 When Creation Of User Incomplete")]
        public async Task POST_Return400WhenCreationOfUserIncomplete()
        {
            HttpResponseMessage response;
            using (var httpClient = new HttpClient())
            {

                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(@"{""user"":""false"",""wrongfield"":""should return error""}", Encoding.UTF8, "application/json");

                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.BadRequest, response.StatusCode);
                }
            }
        }

        [Fact(DisplayName = "POST: Return 200 When Succesfull Login Of User")]
        public async Task POST_Return200WhenSuccesfulLoginOfUser()
        {
            User randomUser = TestFacilitator.RandomUserGenerator();
            UserPostBodyRequest body = new UserPostBodyRequest { UserName = randomUser.UserName, Password = randomUser.UserPassword };
            HttpResponseMessage response;
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");

                    response = await httpClient.SendAsync(request);

                    Assert.True(System.Net.HttpStatusCode.Created.Equals(response.StatusCode));
                }
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user/token"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");

                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.OK, response.StatusCode);
                }
            }
        }
        [Fact(DisplayName = "POST: Return 401 When Incorect Login Of User")]
        public async Task POST_Return401WhenIncorectLoginOfUser()
        {
            User randomUser = TestFacilitator.RandomUserGenerator();
            UserPostBodyRequest body = new UserPostBodyRequest { UserName = randomUser.UserName, Password = randomUser.UserPassword };
            HttpResponseMessage response;
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user/token"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");

                    response = await httpClient.SendAsync(request);

                    Assert.Equal(System.Net.HttpStatusCode.Unauthorized, response.StatusCode);
                }
            }
        }

        [Fact(DisplayName = "GET: Return 200 When User Found")]
        public async Task GET_Return200WhenUserFound()
        {
            User randomUser = TestFacilitator.RandomUserGenerator();
            Guid userId;
            UserPostBodyRequest body = new UserPostBodyRequest { UserName = randomUser.UserName, Password = randomUser.UserPassword };
            HttpResponseMessage response;
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");

                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.Created, response.StatusCode);
                }
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user/token"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");

                    response = await httpClient.SendAsync(request);
                    Assert.True(System.Net.HttpStatusCode.OK.Equals(response.StatusCode));

                    bool correctGuidReturned = Guid.TryParse(response.Content.ReadAsStringAsync().Result.Replace("\"", ""), out userId);
                    Assert.True(correctGuidReturned);

                }

                using (var request = new HttpRequestMessage(new HttpMethod("GET"), $"https://localhost:7210/api/CofeeManagerUser/user/{userId.ToString()}"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                  
                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.OK, response.StatusCode);
                }
            }
        }

        [Fact(DisplayName = "GET: Return 404 When User Not Found")]
        public async Task GET_Return404WhenUserNotFound()
        {
            Guid falseUserguid = Guid.NewGuid();
            HttpResponseMessage response;
            using (var httpClient = new HttpClient())
            {

                using (var request = new HttpRequestMessage(new HttpMethod("Get"), $"https://localhost:7210/api/CofeeManagerUser/user/{falseUserguid}"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.NotFound, response.StatusCode);
                }
            }
        }

        [Fact(DisplayName ="GET: Return 200 When Asking Purse On an User")]
        public async Task GET_Return200WhenAskingPurseOnanUser()
        {
            User randomUser = TestFacilitator.RandomUserGenerator();
            Guid userId;
            UserPostBodyRequest body = new UserPostBodyRequest { UserName = randomUser.UserName, Password = randomUser.UserPassword };
            HttpResponseMessage response;
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");

                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.Created, response.StatusCode);
                }
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user/token"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");

                    response = await httpClient.SendAsync(request);
                    Assert.True(System.Net.HttpStatusCode.OK.Equals(response.StatusCode));

                    bool correctGuidReturned = Guid.TryParse(response.Content.ReadAsStringAsync().Result.Replace("\"", ""), out userId);
                    Assert.True(correctGuidReturned);

                }

                using (var request = new HttpRequestMessage(new HttpMethod("Get"), $"https://localhost:7210/api/CofeeManagerUser/user/{userId}/purse"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);

                    Assert.Equal(HttpStatusCode.OK, response.StatusCode);
                    Assert.Equal("0", response.Content.ReadAsStringAsync().Result);
                }

            }
        }

        [Fact(DisplayName = "GET: Return 404 When Asking Purse On An Inexistant User")]
        public async Task GET_Return404WhenAskingPurseOnAnInexistantUser()
        {
            Guid falseUserguid = Guid.NewGuid();
            HttpResponseMessage response;
            using (var httpClient = new HttpClient())
            {

                using (var request = new HttpRequestMessage(new HttpMethod("Get"), $"https://localhost:7210/api/CofeeManagerUser/user/{falseUserguid}/purse"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.NotFound, response.StatusCode);
                }
            }
        }


        [Fact(DisplayName = "PUT: Return 200 When Purse Of User Increased")]
        public async Task PUT_Return200WhenPurseOfUserIncreased()
        {
            User randomUser = TestFacilitator.RandomUserGenerator();
            decimal amount = TestFacilitator.ProvideRandomPrice();
            Guid userId;
            UserPostBodyRequest body = new UserPostBodyRequest { UserName = randomUser.UserName, Password = randomUser.UserPassword };
            HttpResponseMessage response;
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");

                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.True(System.Net.HttpStatusCode.Created.Equals(response.StatusCode));
                }
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user/token"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");

                    response = await httpClient.SendAsync(request);
                    Assert.True(System.Net.HttpStatusCode.OK.Equals(response.StatusCode));

                    bool correctGuidReturned = Guid.TryParse(response.Content.ReadAsStringAsync().Result.Replace("\"", ""), out userId);
                    Assert.True(correctGuidReturned);
                }

                using (var request = new HttpRequestMessage(new HttpMethod("PUT"), $"https://localhost:7210/api/CofeeManagerUser/user/{userId}/purse/add/{amount}"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");

                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.OK, response.StatusCode);
                }
            }
        }

        [Fact(DisplayName = "PUT: Return 404 When Purse Of Inexistant User Increased")]
        public async Task PUT_Return404WhenPurseOfUserIncreased()
        {
            User randomUser = TestFacilitator.RandomUserGenerator();
            decimal amount = TestFacilitator.ProvideRandomPrice();
            Guid userId = Guid.NewGuid();
            UserPostBodyRequest body = new UserPostBodyRequest { UserName = randomUser.UserName, Password = randomUser.UserPassword };
            HttpResponseMessage response;
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("PUT"), $"https://localhost:7210/api/CofeeManagerUser/user/{userId}/purse/add/{amount}"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");

                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.NotFound, response.StatusCode);
                }
            }
        }

        [Fact(DisplayName = "PUT: Return 200 When Purse Of User Decreased")]
        public async Task PUT_Return200WhenPurseOfUserDecreased()
        {
            User randomUser = TestFacilitator.RandomUserGenerator();
            decimal amount = TestFacilitator.ProvideRandomPrice();
            Guid userId;
            UserPostBodyRequest body = new UserPostBodyRequest { UserName = randomUser.UserName, Password = randomUser.UserPassword };
            HttpResponseMessage response;
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");

                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.True(System.Net.HttpStatusCode.Created.Equals(response.StatusCode));
                }
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user/token"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");

                    response = await httpClient.SendAsync(request);
                    Assert.True(System.Net.HttpStatusCode.OK.Equals(response.StatusCode));

                    bool correctGuidReturned = Guid.TryParse(response.Content.ReadAsStringAsync().Result.Replace("\"", ""), out userId);
                    Assert.True(correctGuidReturned);
                }

                using (var request = new HttpRequestMessage(new HttpMethod("PUT"), $"https://localhost:7210/api/CofeeManagerUser/user/{userId}/purse/remove/0"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");

                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.OK, response.StatusCode);
                }
            }
        }

        [Fact(DisplayName = "PUT: Return 400 When Purse Of User Decreased With Bad Value")]
        public async Task GET_Return400WhenPurseOfUserDecreasedWithBadValue()
        {
            User randomUser = TestFacilitator.RandomUserGenerator();
            decimal amount = TestFacilitator.ProvideRandomPrice();
            Guid userId;
            UserPostBodyRequest body = new UserPostBodyRequest { UserName = randomUser.UserName, Password = randomUser.UserPassword };
            HttpResponseMessage response;
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");

                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.True(System.Net.HttpStatusCode.Created.Equals(response.StatusCode));
                }
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user/token"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");

                    response = await httpClient.SendAsync(request);
                    Assert.True(System.Net.HttpStatusCode.OK.Equals(response.StatusCode));

                    bool correctGuidReturned = Guid.TryParse(response.Content.ReadAsStringAsync().Result.Replace("\"", ""), out userId);
                    Assert.True(correctGuidReturned);
                }

                using (var request = new HttpRequestMessage(new HttpMethod("PUT"), $"https://localhost:7210/api/CofeeManagerUser/user/{userId}/purse/remove/-1"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.BadRequest, response.StatusCode);
                }
            }
        }

        [Fact(DisplayName = "PUT: Return 409 When Purse Of User Cannot Be Decreased Because Of Fund")]
        public async Task PUT_Return409WhenPurseOfUserCannotBeDecreasedBecauseOfFund()
        {
            User randomUser = TestFacilitator.RandomUserGenerator();
            decimal amount = TestFacilitator.ProvideRandomPrice();
            Guid userId;
            UserPostBodyRequest body = new UserPostBodyRequest { UserName = randomUser.UserName, Password = randomUser.UserPassword };
            HttpResponseMessage response;
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");

                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.True(System.Net.HttpStatusCode.Created.Equals(response.StatusCode));
                }
                using (var request = new HttpRequestMessage(new HttpMethod("Post"), "https://localhost:7210/api/CofeeManagerUser/user/token"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");

                    response = await httpClient.SendAsync(request);
                    Assert.True(System.Net.HttpStatusCode.OK.Equals(response.StatusCode));

                    bool correctGuidReturned = Guid.TryParse(response.Content.ReadAsStringAsync().Result.Replace("\"", ""), out userId);
                    Assert.True(correctGuidReturned);
                }

                using (var request = new HttpRequestMessage(new HttpMethod("PUT"), $"https://localhost:7210/api/CofeeManagerUser/user/{userId}/purse/remove/1"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");

                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.Conflict, response.StatusCode);
                }
            }
        }


    }
}
