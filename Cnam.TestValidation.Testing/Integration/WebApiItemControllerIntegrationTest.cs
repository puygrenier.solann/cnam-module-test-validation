﻿using Cnam.TestValidation.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Cnam.TestValidation.Testing.Integration
{
    /// <summary>
    /// Class containing the tests of the correct calls of the API
    /// Can be call using the command line. For more information see dotnet test -h
    /// </summary>
    /// <example>Test can be run using bash : dotnet test --filter "Category=Integration: Item Controller"</example>
    [Trait("Category", "Integration: Item Controller")]
    public class WebApiItemControllerIntegrationTest
    {
        private readonly ITestOutputHelper output;
        public WebApiItemControllerIntegrationTest(ITestOutputHelper outputHelper)
        {
            output = outputHelper;
        }

        [Fact(DisplayName = "POST: Return 201 When Creation Of Item")]
        public async Task Return201WhenCreationOfItem()
        {
            HttpResponseMessage response;
            Item randomItem = TestFacilitator.RandomItemGenerator();
            
            // In production code, don't destroy the HttpClient through using, but better use IHttpClientFactory factory or at least reuse an existing HttpClient instance
            // https://docs.microsoft.com/en-us/aspnet/core/fundamentals/http-requests
            // https://www.aspnetmonsters.com/2016/08/2016-08-27-httpclientwrong/
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("POST"), "https://localhost:7210/api/CofeeManagerItems/item"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(randomItem.ToString(), Encoding.UTF8, "application/json");

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body :\n" + request.Content.ReadAsStringAsync().Result);
                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Response :\n" + response.ToString());
                    string jsonResponse = response.Content.ReadAsStringAsync().Result;
                    output.WriteLine("Response body:\n" + jsonResponse);

                    Assert.True(response.StatusCode is System.Net.HttpStatusCode.Created);
                    Assert.Contains("SUCCES", jsonResponse);
                }

                //Clean Test (1/2) : Get Id
                Guid idItem;
                using (var request = new HttpRequestMessage(new HttpMethod("GET"), "https://localhost:7210/api/CofeeManagerItems/item/all"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);
                    var allItems = response.Content.ReadAsStringAsync().Result;
                    Item[]? deserializedObject = JsonConvert.DeserializeObject<Item[]?>(response.Content.ReadAsStringAsync().Result);
                    idItem = deserializedObject.Where(item => item.Equals(randomItem)).First().Id;
                }
                //Clean Test (2/2): Delete Item
                using (var request = new HttpRequestMessage(new HttpMethod("DELETE"), $"https://localhost:7210/api/CofeeManagerItems/item/{idItem}"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);
                }
            }
        }

        [Fact(DisplayName = "POST: Return 409 When Creation Of Item Twice")]
        public async Task Return409WhenCreationOfItem()
        {
            HttpResponseMessage response;
            Item randomItem = TestFacilitator.RandomItemGenerator();
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("POST"), "https://localhost:7210/api/CofeeManagerItems/item"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");

                    request.Content = new StringContent(randomItem.ToString(),
                        Encoding.UTF8,
                        "application/json");

                    response = await httpClient.SendAsync(request);
                    string jsonResponse = response.Content.ReadAsStringAsync().Result;

                    Assert.Equal(System.Net.HttpStatusCode.Created, response.StatusCode);
                    Assert.Contains("SUCCES", jsonResponse);
                }
            
                using (var request = new HttpRequestMessage(new HttpMethod("POST"), "https://localhost:7210/api/CofeeManagerItems/item"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");

                    request.Content = new StringContent(randomItem.ToString(),
                        Encoding.UTF8,
                        "application/json");

                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body:\n" + request.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Response :\n" + response.ToString());
                    output.WriteLine("Response body:\n" + response.Content.ReadAsStringAsync().Result);
                    Assert.Equal(System.Net.HttpStatusCode.Conflict, response.StatusCode);
                    //Assert.Contains("SUCCES", jsonResponse);
                }

                //Clean Test (1/2) : Get Id
                Guid idItem;
                using (var request = new HttpRequestMessage(new HttpMethod("GET"), "https://localhost:7210/api/CofeeManagerItems/item/all"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);
                    var allItems = response.Content.ReadAsStringAsync().Result;
                    Item[]? deserializedObject = JsonConvert.DeserializeObject<Item[]?>(response.Content.ReadAsStringAsync().Result);
                    idItem = deserializedObject.Where(item => item.Equals(randomItem)).First().Id;
                }
                //Clean Test (2/2): Delete Item
                using (var request = new HttpRequestMessage(new HttpMethod("DELETE"), $"https://localhost:7210/api/CofeeManagerItems/item/{idItem}"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);
                }
            }


        }

        [Fact(DisplayName = "POST: Return 400 When Item Request Not Well Formated")]
        public async Task Return400WhenItemRequestNotWellFormated()
        {
            HttpResponseMessage response;
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("POST"), "https://localhost:7210/api/CofeeManagerItems/item"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");

                    request.Content = new StringContent("",
                        Encoding.UTF8,
                        "application/json");

                    response = await httpClient.SendAsync(request);
                    string jsonResponse = response.Content.ReadAsStringAsync().Result;

                    Assert.Equal(System.Net.HttpStatusCode.BadRequest, response.StatusCode);
                }

                using (var request = new HttpRequestMessage(new HttpMethod("POST"), "https://localhost:7210/api/CofeeManagerItems/item"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    Item incompleteItem = new Item { Price = 10m };
                    request.Content = new StringContent(incompleteItem.ToString(),
                        Encoding.UTF8,
                        "application/json");

                    response = await httpClient.SendAsync(request);
                    string jsonResponse = response.Content.ReadAsStringAsync().Result;

                    Assert.Equal(System.Net.HttpStatusCode.BadRequest  , response.StatusCode);
                }
            }
        }

        [Fact(DisplayName = "GET: Return 200 When Getting All Item")]
        public async Task Return200WhenGettingAllItem()
        {
            HttpResponseMessage response;
            Item randomItem = TestFacilitator.RandomItemGenerator();
            Guid idItem;
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("GET"), "https://localhost:7210/api/CofeeManagerItems/item/all"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Response :\n" + response.ToString());
                    string jsonResponse = response.Content.ReadAsStringAsync().Result;
                    output.WriteLine("Response body:\n" + jsonResponse);
                    Assert.Equal(System.Net.HttpStatusCode.OK,response.StatusCode);
                }
            }
        }

        [Fact (DisplayName ="GET: Return 200 When Item Is Found")]
        public async Task Return200WhenItemIsFound()
        {
            HttpResponseMessage response;
            Item randomItem = TestFacilitator.RandomItemGenerator();
            Guid idItem;
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("POST"), "https://localhost:7210/api/CofeeManagerItems/item"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(randomItem.ToString(), Encoding.UTF8, "application/json");
                    response = await httpClient.SendAsync(request);
                }

                using (var request = new HttpRequestMessage(new HttpMethod("GET"), "https://localhost:7210/api/CofeeManagerItems/item/all"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);
                    var allItems = response.Content.ReadAsStringAsync().Result;
                    Item[]? deserializedObject = JsonConvert.DeserializeObject<Item[]?>(response.Content.ReadAsStringAsync().Result);
                    idItem = deserializedObject.Where(item => item.Equals(randomItem)).First().Id;
                }

                using (var request = new HttpRequestMessage(new HttpMethod("GET"), $"https://localhost:7210/api/CofeeManagerItems/item/{idItem}"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Response :\n" + response.ToString());
                    string jsonResponse = response.Content.ReadAsStringAsync().Result;
                    output.WriteLine("Response body:\n" + jsonResponse);
                    Assert.Equal(System.Net.HttpStatusCode.OK, response.StatusCode);
                }

                //Clean Test (1/2) : Get Id
                using (var request = new HttpRequestMessage(new HttpMethod("GET"), "https://localhost:7210/api/CofeeManagerItems/item/all"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);
                    var allItems = response.Content.ReadAsStringAsync().Result;

                    Item[]? deserializedObject = JsonConvert.DeserializeObject<Item[]?>(response.Content.ReadAsStringAsync().Result);
                    idItem = deserializedObject.Where(item => item.Equals(randomItem)).First().Id;
                }
                //Clean Test (2/2): Delete Item
                using (var request = new HttpRequestMessage(new HttpMethod("DELETE"), $"https://localhost:7210/api/CofeeManagerItems/item/{idItem}"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);
                }
            }
        }

        [Fact(DisplayName = "GET: Return 404 When Item Is Not Found")]
        public async Task Return404WhenItemIsNotFound()
        {
            HttpResponseMessage response;
            Guid guid = Guid.NewGuid();
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("GET"), $"https://localhost:7210/api/CofeeManagerItems/item/{guid}"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Response :\n" + response.ToString());
                    string jsonResponse = response.Content.ReadAsStringAsync().Result;
                    output.WriteLine("Response body:\n" + jsonResponse);
                    Assert.Equal(System.Net.HttpStatusCode.NotFound, response.StatusCode);
                }
            }
        }

        [Fact(DisplayName = "GET: Return 400 When Providing Wrong Formated Input For Item")]
        public async Task Return400WhenProvidingWrongFormatedInputForItem()
        {
            string randomString = Path.GetRandomFileName();
            HttpResponseMessage response;
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("GET"), $"https://localhost:7210/api/CofeeManagerItems/item/{randomString}"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Response :\n" + response.ToString());
                    string jsonResponse = response.Content.ReadAsStringAsync().Result;
                    output.WriteLine("Response body:\n" + jsonResponse);
                    Assert.Equal(System.Net.HttpStatusCode.BadRequest, response.StatusCode);

                }
            }
        }

        [Fact(DisplayName = "PUT: Return 200 When Item Is Update")]
        public async Task PUT_Return200WhenItemIsUpdate()
        {
            HttpResponseMessage response;
            Item randomItem = TestFacilitator.RandomItemGenerator();
            Item randomItemValueToProvideForUpdate = TestFacilitator.RandomItemGenerator();
            Guid idItem;
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("POST"), "https://localhost:7210/api/CofeeManagerItems/item"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(randomItem.ToString(), Encoding.UTF8, "application/json");
                    response = await httpClient.SendAsync(request);
                }

                using (var request = new HttpRequestMessage(new HttpMethod("GET"), "https://localhost:7210/api/CofeeManagerItems/item/all"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);
                    var allItems = response.Content.ReadAsStringAsync().Result;
                    Item[]? deserializedObject = JsonConvert.DeserializeObject<Item[]?>(response.Content.ReadAsStringAsync().Result);
                    idItem = deserializedObject.Where(item => item.Equals(randomItem)).First().Id;
                }

                using (var request = new HttpRequestMessage(new HttpMethod("PUT"), "https://localhost:7210/api/CofeeManagerItems/item"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");

                    request.Content = new StringContent(new Item { Id = idItem, Price= randomItemValueToProvideForUpdate .Price,Description = randomItemValueToProvideForUpdate.Description, EAN= randomItemValueToProvideForUpdate.EAN, Title = randomItemValueToProvideForUpdate .Title}.ToString());
                    request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
                    response = await httpClient.SendAsync(request);

                    Assert.Equal(System.Net.HttpStatusCode.OK, response.StatusCode);
                }

                //Clean Test : Delete Item
                using (var request = new HttpRequestMessage(new HttpMethod("DELETE"), $"https://localhost:7210/api/CofeeManagerItems/item/{idItem}"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);
                }
            }
        }

        [Fact(DisplayName = "PUT: Return 404 When Item Is Not Found")]
        public async Task PUT_Return404WhenItemIsNotFound()
        {
            HttpResponseMessage response;
            Item randomItem = TestFacilitator.RandomItemGenerator();
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("PUT"), "https://localhost:7210/api/CofeeManagerItems/item"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");

                    request.Content = new StringContent(randomItem.ToString());
                    request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Response :\n" + response.ToString());
                    string jsonResponse = response.Content.ReadAsStringAsync().Result;
                    output.WriteLine("Response body:\n" + jsonResponse);
                    Assert.Equal(System.Net.HttpStatusCode.NotFound, response.StatusCode);
                }
            }
        }


        [Fact(DisplayName = "DELETE: Return 200 When Item Is Deleted")]
        public async Task Return200WhenItemIsDeleted()
        {
            HttpResponseMessage response;
            Item randomItem = TestFacilitator.RandomItemGenerator();
            Guid idItem;
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("POST"), "https://localhost:7210/api/CofeeManagerItems/item"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    request.Content = new StringContent(randomItem.ToString(), Encoding.UTF8, "application/json");

                    output.WriteLine("Request :\n" + request.ToString());
                    output.WriteLine("Request body :\n" + request.Content.ReadAsStringAsync().Result);
                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Response :\n" + response.ToString());
                    string jsonResponse = response.Content.ReadAsStringAsync().Result;
                    output.WriteLine("Response body:\n" + jsonResponse);
                    Assert.True(response.StatusCode is System.Net.HttpStatusCode.Created);
                }

                using (var request = new HttpRequestMessage(new HttpMethod("GET"), "https://localhost:7210/api/CofeeManagerItems/item/all"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);
                    var allItems = response.Content.ReadAsStringAsync().Result;


                    Item[]? deserializedObject = JsonConvert.DeserializeObject<Item[]?>(response.Content.ReadAsStringAsync().Result);
                    idItem = deserializedObject.Where(item => item.Equals(randomItem)).First().Id;

                    output.WriteLine("Response :\n" + response.ToString());
                    string jsonResponse = response.Content.ReadAsStringAsync().Result;
                    output.WriteLine("Response body:\n" + jsonResponse);
                    Assert.True(response.StatusCode is System.Net.HttpStatusCode.OK);
                }

                using (var request = new HttpRequestMessage(new HttpMethod("DELETE"), $"https://localhost:7210/api/CofeeManagerItems/item/{idItem}"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Response :\n" + response.ToString());
                    string jsonResponse = response.Content.ReadAsStringAsync().Result;
                    output.WriteLine("Response body:\n" + jsonResponse);
                    Assert.Equal(System.Net.HttpStatusCode.OK, response.StatusCode);
                    Assert.Contains("SUCCES", jsonResponse);
                }
            }
        }
    
        [Fact(DisplayName = "DELETE: Return 404 When Item Is Not Found")]
        public async Task Delete_Return404WhenItemIsNotFound()
        {
            HttpResponseMessage response;
            Guid guid = Guid.NewGuid();
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("DELETE"), $"https://localhost:7210/api/CofeeManagerItems/item/{guid}"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "*/*");
                    response = await httpClient.SendAsync(request);

                    output.WriteLine("Response :\n" + response.ToString());
                    string jsonResponse = response.Content.ReadAsStringAsync().Result;
                    output.WriteLine("Response body:\n" + jsonResponse);
                    Assert.Equal(System.Net.HttpStatusCode.NotFound, response.StatusCode);
                }
            }
        }

    }
}

